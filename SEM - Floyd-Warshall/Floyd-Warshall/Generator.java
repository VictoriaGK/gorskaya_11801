import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Generator {

    public static void gen() throws IOException {
        File newFile = new File( "edge.txt");
        FileWriter fileReader = new FileWriter(newFile);
        BufferedWriter bufferedWriter = new BufferedWriter(fileReader);

        int a = 50 +  (int)(Math.random()*50);
        bufferedWriter.write(Integer.toString(a));
        bufferedWriter.newLine();

        for (int i = 0; i < a ; i++) {
            int n = 10 +  (int)(Math.random()*90);
            bufferedWriter.write(Integer.toString(n));
            bufferedWriter.newLine();

            for (int j = 0; j <n*n ; j++) {
                bufferedWriter.write(Integer.toString((int)(Math.random()*100)));
                bufferedWriter.write(" ");
            }
            bufferedWriter.newLine();

        }
        bufferedWriter.close();

    }




}
