public class Edge {
    private double weight;

    public Edge(double weight) {
        this.weight = weight;
    }

    public  double getWeight() {
        return weight;
    }
    public Edge() {
        weight = 0;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Edge(String weight) {
        if(weight.equals("inf")) {
            this.weight = Double.POSITIVE_INFINITY;
        }else{
            this.weight = Double.parseDouble(weight);
        }
    }

    @Override
    public String toString() {
        return ""
                 + weight +
                "";
    }
}
