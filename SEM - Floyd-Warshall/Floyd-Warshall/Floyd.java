import java.io.FileNotFoundException;
import java.util.LinkedList;

public class Floyd {



    public static double[][] floydMass(double [][] mass, int n){
        int count = 0;
        for (int k = 0; k <n ; k++) {
            for (int i = 0; i <mass.length ; i++) {
                for (int j = 0; j <mass.length ; j++) {
                    if(mass[i][j] >= (mass[k][j] + mass[i][k])){
                        mass[i][j] = mass[k][j] + mass[i][k];
                    } count++;
                }

            }

        }
        System.out.println(count);
        return mass;
    }

    public static LinkedList<Edge> floydLinked(LinkedList<Edge> mass, int n){
        int count = 0;
        for (int k = 0; k <n ; k++) {
            for (int i = 0; i <n ; i++) {
                for (int j = 0; j <n ; j++) {
                    if((mass.get(i*n + j)).getWeight() >= ((mass.get(k*n + j)).getWeight() + (mass.get(i*n + k)).getWeight())){
                        mass.get(i*n + j).setWeight((mass.get(k*n + j).getWeight() + mass.get(i*n + k).getWeight()));
                    }
                    count++;
                }

            }

        }
        System.out.println(count);
        return mass;
    }
}
