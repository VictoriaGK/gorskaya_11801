import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {


       // Generator.gen();
        Scanner sc = new Scanner(new File("edge.txt"));
        int a = Integer.parseInt(sc.nextLine());
        long time = 0;
        while (sc.hasNextLine()) {

            int n = Integer.parseInt(sc.nextLine());
            LinkedList<Edge> edges = Reader.reader1(sc, n);
            System.out.println(" ");
            System.out.println("linked");

            long h = System.nanoTime();
            Floyd.floydLinked(edges, n);
            long g = System.nanoTime();
            time = time + g  - h;

            System.out.println(g - h);

        }

        sc = new Scanner(new File("edge.txt"));
        a = Integer.parseInt(sc.nextLine());
        long time1 = 0;
        while (sc.hasNextLine()) {

            int n = Integer.parseInt(sc.nextLine());
            double mass3[][] = Reader.reader(sc, n);
            System.out.println(" ");
            System.out.println("mass ");


            long h = System.nanoTime();
            Floyd.floydMass(mass3, n);
            long g = System.nanoTime();
            time1 = time1 + g  - h;
            System.out.println(g - h);

        }


        System.out.println(time - time1);










    }
}
