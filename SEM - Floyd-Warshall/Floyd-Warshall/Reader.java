import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Reader {



    public static LinkedList<Edge> reader1(Scanner sc, int n) throws FileNotFoundException {

        LinkedList<Edge> edges = new LinkedList<>();

            String[] s = sc.nextLine().split("\\s+");
            for (int i = 0; i <n*n; i++) {
                if ((i % (n+1)) == 0){
                    Edge edge = new Edge();
                    edges.add(edge);
                }
                else {
                    Edge edge = new Edge(s[i]);
                    edges.add(edge);
                }




        } return edges;

    }

    public static double[][]reader(Scanner sc, int n) throws FileNotFoundException {

        double[][] mass = new double[n][n];
            String[] s = sc.nextLine().split("\\s+");
            for (int i = 0; i <n; i++) {
                for (int j = 0; j <n; j++) {
                    if(j == i){
                        mass[i][j] = 0;
                    }else{
                        if(s[i*n + j].equals("inf")){
                            mass[i][j] = Double.POSITIVE_INFINITY;
                        }
                        else{
                            mass[i][j] = Integer.parseInt(s[i*n + j]);
                        }
                    }



            }

        } return mass;

    }






}
