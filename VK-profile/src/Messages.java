public class Messages {
	private String text,date;
	private Profile profileTo, profileFrom;
	private boolean checkRead;

	public Messages(Profile profileTo, Profile profileFrom, String text, String date, boolean checkRead) {
		this.text = text;
		this.date = date;
		this.profileTo = profileTo;
		this.profileFrom = profileFrom;
		this.checkRead = checkRead;
	}
	public String getText() {
		return text;
	}
	public String getDate() {
		return date;
	}

	public Profile getProfileFrom() {
		return profileFrom;
	}

	public Profile getProfileTo() {
		return profileTo;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setProfileTo(Profile profileTo) {
		this.profileTo = profileTo;
	}

	public void setProfileFrom(Profile profileFrom) {
		this.profileFrom = profileFrom;
	}

	public boolean isCheckRead() {
		return checkRead;
	}

	public void setCheckRead(boolean checkRead) {
		this.checkRead = checkRead;
	}

	@Override
	public String toString() {
		return "Messages{" +
				"text='" + text + '\'' +
				", date='" + date + '\'' +
				", profileTo=" + profileTo +
				", profileFrom=" + profileFrom +
				", checkRead='" + checkRead + '\'' +
				'}';
	}
}