abstract class Places<T> {
    private int fromYear, toYear;
    Profile profile;
    private T place;
    public Places(T place, Profile profile, int fromYear, int toYear){
        this.place = place;
        this.profile = profile;
        this.fromYear = fromYear;
        this.toYear = toYear;
    }
    public Places(){ }

    public int getFromYear() {
        return fromYear;
    }

    public int getToYear() {
        return toYear;
    }

    public Profile getProfile() {
        return profile;
    }

    public T getPlace() {
        return place;
    }
}
