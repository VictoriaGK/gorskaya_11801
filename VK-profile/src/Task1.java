import java.util.ArrayList;

public class Task1 {
    public static boolean p(Profile profile, int u, ArrayList<Messages> messages, ArrayList<Post> posts){
        return numMessages(profile,u,messages)>= numPosts(profile,u,posts);
    }

    public static int numMessages(Profile profile, int u, ArrayList<Messages> messages){
        int k = 0;
        for(Messages message: messages){
            if((message.getProfileFrom().getId() == profile.getId()) && (message.getProfileTo().getId() == u)){
                k++;
            }
        }
        return k;
    }

    public static int numPosts(Profile profile, int u, ArrayList<Post> posts){
        int k = 0;
        for(Post post: posts){
            if((post.getFromWhom().getId() == profile.getId()) && (post.getToWhom().getId() == u)){
                k++;
            }
        }
        return k;
    }
}
