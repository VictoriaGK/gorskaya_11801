public class Profile {
    private String firstName, lastName, familyStatus, pageAdress, status, gender;
    private int id;

    public Profile(String firstName,
                   String lastName,
                   String pageAdress,
                   String familyStatus,
                   String status,
                   int id,
                   String gender) {
        this.firstName = firstName;
        this.familyStatus = familyStatus;
        this.id = id;
        this.lastName = lastName;
        this.status = status;
        this.gender = gender;
        this.pageAdress = pageAdress;
    }


    public String getFamilyStatus() {
        return familyStatus;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public void setPageAdress(String pageAdress) {
        this.pageAdress = pageAdress;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getPageAdress() {
        return pageAdress;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getGender() {
        return gender;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "name='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", familyStatus='" + familyStatus + '\'' +
                ", pageAdress='" + pageAdress + '\'' +
                ", status='" + status + '\'' +
                ", gender='" + gender + '\'' +
                ", id=" + id +
                '}';
    }
}
