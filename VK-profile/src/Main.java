import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args)
            throws IOException {

        Scanner sc = null;
        sc = new Scanner(new File("files/profile.txt"));
        ArrayList<Profile> profiles = new ArrayList<Profile>();
        ArrayList<Like> likes = new ArrayList<Like>();
        ArrayList<Post> posts = new ArrayList<>();
        ArrayList<Post> posts2 = new ArrayList<>();
        ArrayList<City> cities = new ArrayList<>();
        ArrayList<Job> jobs = new ArrayList<>();
        ArrayList<School> schools = new ArrayList<>();
        ArrayList<University> universities = new ArrayList<>();
        ArrayList<Universities> universitiesArrayList = new ArrayList<>();
        ArrayList<Schools> schoolArrayList = new ArrayList<>();
        ArrayList<Jobs> jobsArrayList = new ArrayList<>();
        ArrayList<Cities> citiesArrayList = new ArrayList<>();
        while (sc.hasNextLine()) {

            String line = sc.nextLine();
            String[] s = line.trim().split("\t");
            String firstName = s[0];
            String lastName = s[1];
            String familyStatus = s[2];
            String pageAdress = s[3];
            String status = s[4];
            int id = Integer.parseInt(s[5]);
            String gender = s[6];
            profiles.add(new Profile(firstName, lastName, pageAdress, familyStatus, status, id, gender));
        }
        sc = new Scanner(new File("files/Messages.txt"));
        ArrayList<Messages> messages = new ArrayList<>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] s = line.trim().split("\t");
            Profile profileTo = null;
            Profile profileFrom = null;
            boolean checkRead = false;
            if (Integer.parseInt(s[4]) == 1) {
                checkRead = true;
            }
            for (Profile profile : profiles) {
                if (Integer.parseInt(s[0]) == profile.getId()) {
                    profileTo = profile;
                }
                if (Integer.parseInt(s[1]) == profile.getId()) {
                    profileFrom = profile;
                }
            }
            messages.add(new Messages(profileTo, profileFrom, s[2], s[3], checkRead));
        }
        sc = new Scanner(new File("files/Posts.txt"));

        while (sc.hasNextLine()) {
            String[] newPost = sc.nextLine().split("\t");
            Profile profileTo = null;
            Profile profileFrom = null;
            Post post = null;
            for (Profile profile : profiles) {
                if (Integer.parseInt(newPost[2]) == profile.getId()) {
                    profileTo = profile;
                }
                if (Integer.parseInt(newPost[3]) == profile.getId()) {
                    profileFrom = profile;
                }
            }
            for (Post p : posts) {
                if ((Integer.parseInt(newPost[5]) == p.getId())) {
                    post = p;
                }
            }
            posts.add(new Post(newPost[0], newPost[1], profileTo, profileFrom, Integer.parseInt(newPost[4])));
        }
        sc = new Scanner(new File("files/Posts.txt"));
        while (sc.hasNextLine()) {
            String[] newPost = sc.nextLine().split("\t");
            Profile profileTo = null;
            Profile profileFrom = null;
            Post post = null;
            for (Profile profile : profiles) {
                if (Integer.parseInt(newPost[2]) == profile.getId()) {
                    profileTo = profile;
                }
                if (Integer.parseInt(newPost[3]) == profile.getId()) {
                    profileFrom = profile;
                }
            }
            for (Post p : posts) {
                if ((Integer.parseInt(newPost[5]) == p.getId())) {
                    post = p;
                }
            }
            posts2.add(new Post(newPost[0], newPost[1], profileTo, profileFrom, Integer.parseInt(newPost[4]), post));
        }
        sc = new Scanner(new File("files/Like.txt"));
        while (sc.hasNextLine()) {
            String[] newLike = sc.nextLine().split("\t");
            Post post = null;
            Profile profileFrom = null;
            for (Profile profile : profiles) {
                if (Integer.parseInt(newLike[0]) == profile.getId()) {
                    profileFrom = profile;
                }
            }
            for (Post p : posts) {
                if (p.getId() == Integer.parseInt(newLike[1])) {
                    post = p;
                }
            }
            likes.add(new Like(post, profileFrom));

        }
        sc = new Scanner(new File("files/Cities.txt"));

        while (sc.hasNextLine()) {
            String[] newCity = sc.nextLine().split("\t");
            cities.add(new City(newCity[0], Integer.parseInt(newCity[1])));

        }
        sc = new Scanner(new File("files/Jobs.txt"));
        while (sc.hasNextLine()) {
            String[] newJob = sc.nextLine().split("\t");
            jobs.add(new Job(newJob[0], Integer.parseInt(newJob[1])));
        }
        sc = new Scanner(new File("files/Schools.txt"));
        while (sc.hasNextLine()) {
            String[] newSchool = sc.nextLine().split("\t");
            schools.add(new School(newSchool[0], Integer.parseInt(newSchool[1])));
        }
        sc = new Scanner(new File("files/Universities.txt"));
        while (sc.hasNextLine()) {
            String[] newUniversity = sc.nextLine().split("\t");
            universities.add(new University(newUniversity[0], Integer.parseInt(newUniversity[1])));
        }
        sc = new Scanner(new File("files/University-Profile.txt"));
        while (sc.hasNextLine()) {
            String[] newUniversity = sc.nextLine().split("\t");
            University university = null;
            Profile profile = null;
            for (University u : universities) {
                if (u.getId() == Integer.parseInt(newUniversity[0])) {
                    university = u;
                }
            }
            for (Profile p : profiles) {
                if (p.getId() == Integer.parseInt(newUniversity[1])) {
                    profile = p;
                }
            }
            universitiesArrayList.add(new Universities(university, profile, Integer.parseInt(newUniversity[2]),
                    Integer.parseInt(newUniversity[3])));
        }

        sc = new Scanner(new File("files/School-Profile.txt"));
        while (sc.hasNextLine()) {
            String[] newSchool = sc.nextLine().split("\t");
            School school = null;
            Profile profile = null;
            for (School s : schools) {
                if (s.getId() == Integer.parseInt(newSchool[0])) {
                    school = s;
                }
            }
            for (Profile profile1 : profiles) {
                if (profile1.getId() == Integer.parseInt(newSchool[1])) {
                    profile = profile1;
                }
            }
            schoolArrayList.add(new Schools(school, profile, Integer.parseInt(newSchool[2]),
                    Integer.parseInt(newSchool[3])));
        }


        sc = new Scanner(new File("files/Job-Profile.txt"));
        while (sc.hasNextLine()) {
            String[] newJob = sc.nextLine().split("\t");
            Job job = null;
            Profile profile = null;
            for (Job j : jobs) {
                if (j.getId() == Integer.parseInt(newJob[0])) {
                    job = j;
                }
            }
            for (Profile profile1 : profiles) {
                if (profile1.getId() == Integer.parseInt(newJob[1])) {
                    profile = profile1;
                }
            }
            jobsArrayList.add(new Jobs(job, profile, Integer.parseInt(newJob[2]),
                    Integer.parseInt(newJob[3])));
        }

        sc = new Scanner(new File("files/City-Profile.txt"));
        while (sc.hasNextLine()) {
            String[] newJob = sc.nextLine().split("\t");
            City city = null;
            Profile profile = null;
            for (City c : cities) {
                if (c.getId() == Integer.parseInt(newJob[0])) {
                    city = c;
                }
            }
            for (Profile profile1 : profiles) {
                if (profile1.getId() == Integer.parseInt(newJob[1])) {
                    profile = profile1;
                }
            }
            citiesArrayList.add(new Cities(city, profile, Integer.parseInt(newJob[2]),
                    Integer.parseInt(newJob[3])));
        }

        for(Profile profile: profiles){
            int u = 3;
            if(!(profile.getId()==u)){
                if(Task1.p(profile,u,messages,posts))
                System.out.println(profile.getFirstName());
            }

        }



    }



}
