public class Post{

	private Post post;
	private String text;
	private String date;
	private Profile toWhom;
	private Profile fromWhom;
	private int id;

	
	public Post(String text, String date, Profile toWhom, Profile fromWhom, int id, Post post){
		this.date = date;
		this.post = post;
		this.toWhom = toWhom;
		this.fromWhom = fromWhom;
		this.id = id;
		this.text = text;
	}

	public Post(String text, String date, Profile toWhom, Profile fromWhom, int id){
		this.date = date;
		this.text = text;
		this.toWhom = toWhom;
		this.fromWhom = fromWhom;
		this.id = id;
		this.post = null;
	}


	public String getDate(){
		return date;
	}

	public Profile getFromWhom() {
		return fromWhom;
	}

	public Profile getToWhom() {
		return toWhom;
	}

	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setToWhom(Profile toWhom) {
		this.toWhom = toWhom;
	}

	public void setFromWhom(Profile fromWhom) {
		this.fromWhom = fromWhom;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	@Override
	public String toString() {
		return "Post{" +
				"post=" + post +
				", text='" + text + '\'' +
				", date='" + date + '\'' +
				", toWhom=" + toWhom +
				", fromWhom=" + fromWhom +
				", id=" + id +
				'}';
	}
}