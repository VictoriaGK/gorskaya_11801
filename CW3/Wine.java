//вар2
public class Wine {
    private Grape grape;
    private String winery;
    private Appelation appelation;
    private String state;
    private String name;
    private int year;
    private int price;
    private int score;
    private long cases;
    private String drink;

    public String getWinery() {
        return winery;
    }

    public Appelation getAppelation() {
        return appelation;
    }

    public void setCases(long cases) {
        this.cases = cases;
    }

    public Wine(String winery, String state, String name, int year, int price, int score, String drink) {
        this.winery = winery;
        this.state = state;
        this.name = name;
        this.year = year;
        this.price = price;
        this.score = score;
        this.drink = drink;
    }

    public void setGrape(Grape grape) {
        this.grape = grape;
    }

    public void setAppelation(Appelation appelation) {
        this.appelation = appelation;
    }

    @Override
    public String toString() {
        return "Wine{" +
                "grape=" + grape +
                ", winery='" + winery + '\'' +
                ", appelation=" + appelation +
                ", state='" + state + '\'' +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", score=" + score +
                ", cases=" + cases +
                ", drink='" + drink + '\'' +
                '}';
    }
}