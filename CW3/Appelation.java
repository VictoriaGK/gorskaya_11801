//вар2
public class Appelation {
    private String appelation;
    private String country;
    private String state;
    private String area;
    private String isAVA;

    public Appelation(String appelation, String country, String state, String area, String isAVA) {
        this.appelation = appelation;
        this.country = country;
        this.state = state;
        this.area = area;
        this.isAVA = isAVA;
    }

    @Override
    public String toString() {
        return "Appelation{" +
                "appelation='" + appelation + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                ", area='" + area + '\'' +
                ", isAVA='" + isAVA + '\'' +
                '}';
    }

    public String getAppelation() {
        return appelation;
    }

    public void setAppelation(String appelation) {
        this.appelation = appelation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getIsAVA() {
        return isAVA;
    }

    public void setIsAVA(String isAVA) {
        this.isAVA = isAVA;
    }
}
