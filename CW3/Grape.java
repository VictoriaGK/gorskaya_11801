//вар2
public class Grape {
    private int id;
    private String grape;
    private String color;

    public Grape(int id, String grape, String color) {
        this.id = id;
        this.grape = grape;
        this.color = color;
    }
    @Override
    public String toString() {
        return "Grape{" +
                "id=" + id +
                ", grape='" + grape + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGrape() {
        return grape;
    }

    public void setGrape(String grape) {
        this.grape = grape;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}