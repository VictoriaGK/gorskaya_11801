//вар2

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        ArrayList<Grape> grapes = new ArrayList<Grape>();




        Scanner sc = new Scanner(new File("grapes.csv"));
        sc.nextLine();
        while (sc.hasNextLine()){
            String[] s = sc.nextLine().split(",");
            Grape grape = new Grape(Integer.parseInt(s[0]), s[1].substring(1,s[1].length()-1), s[2].substring(1,s[2].length()-1));
            grapes.add(grape);
        }

        ArrayList<Appelation> appelations = new ArrayList<Appelation>();
        sc = new Scanner(new File("appellations.csv"));
        sc.nextLine();
        while (sc.hasNextLine()){
            String[] s = sc.nextLine().split(",");
            Appelation appelation = new Appelation(s[1].substring(1,s[1].length()-1),s[2].substring(1,s[2].length()-1),s[3].substring(1,s[3].length()-1),
                    s[4].substring(1,s[4].length()-1),s[5].substring(1,s[5].length()-1));
            appelations.add(appelation);

        }
        ArrayList<Wine> wines = new ArrayList<Wine>();
        sc = new Scanner(new File("wine.csv"));
        sc.nextLine();
        while (sc.hasNextLine()){
            String[] s = sc.nextLine().split(",");
            Wine wine = new Wine((s[2].substring(1,s[2].length()-1)), s[4].substring(1,s[4].length()-1), s[5].substring(1,s[5].length()-1), Integer.parseInt(s[6]),Integer.parseInt(s[7]),Integer.parseInt(s[8]), s[10].substring(1,s[10].length()-1));
            for(Grape grape: grapes){
                if(s[1].substring(1,s[1].length()-1).equals(grape.getGrape())){
                    wine.setGrape(grape);
                }
            }
            for(Appelation appelation: appelations){
                if(s[3].substring(1,s[3].length()-1).equals(appelation.getAppelation())){
                    wine.setAppelation(appelation);
                }
            }
            if(!(s[9].equals("NULL"))){
                wine.setCases(Long.parseLong(s[9]));
            }else {
                wine.setCases(0);
            }


            wines.add(wine);
        }
//        for(Wine wine: wines) {
//            System.out.println(wine);
//        }
//        for(Appelation appelation: appelations){
//            System.out.println(appelation);
//        }
        System.out.println(maxCountWineInAppelation(appelations,wines));
        countWine(wines, grapes);
    }
    public static void countWine(ArrayList<Wine> wines, ArrayList<Grape> grapes){
        for(Grape grape: grapes){
            int k = 0;
            for (Wine wine: wines){
                if(wine.getGrape().getGrape().equals(grape.getGrape())){
                    k++;

                }
            }
            System.out.println(grape.getGrape());
            System.out.println(k);

        }

    }



    public static Appelation maxCountWineInAppelation(ArrayList<Appelation> appelations, ArrayList<Wine> wines){
        int max = -1;
        Appelation maxWineAppelation = null;
        for (Appelation appelation: appelations) {
            int k = 0;
            for (Wine wine : wines) {
                if (wine.getAppelation().equals(appelation)) {
                    k++;
                }
            }
            if (k > max){
                max = k;
                maxWineAppelation = appelation;
            }
        }return maxWineAppelation;
    }
}
