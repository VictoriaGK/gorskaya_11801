/**
* @author Victoria Gorskaya
* 11-801
* Task 11 
*/
import java.util.*;
import java.lang.Math.*;

public class Task11{
	public static void main(String[] args){
		int k = 4;
		int n = 22;
		int i = 1;
		int n10 = 0;

		while (n != 0){
			n10 = n10 + (n % 10) * i;
			i= i*k;
			n = n / 10;
		}
		System.out.println(n10);

	}
}