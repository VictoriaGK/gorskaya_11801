/**
* @author Victoria Gorskaya
* 11-801
* Task 13 
*/
import java.util.*;
import java.lang.Math.*;

public class Task13{
	public static void main(String[] args){
		int [] a = {958, 232, 322, 34, 185, 233, 1111111, 111211};
		int i = 1;
		int n = a.length;
		boolean flag = true;
		int m = 0;
		
		while(i < n-1 && flag){
			if (a[i] > a[i-1] && a[i]>a[i+1] && (a[i] % 2 == 0)){
				m++;
			} 
			i++;
			if (m > 2) {
				flag = false;
			} 
		}
		if (m != 2) {
				flag = false;
			} 		

		System.out.println(flag);
	
	}

}