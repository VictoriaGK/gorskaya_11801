/**
* @author Victoria Gorskaya
* 11-801
* Task 02 
*/
import java.util.*;
import java.lang.Math.*;

public class Task02{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		int n = sc.nextInt();
		int i = x+1;
	while (i < n) {
			if ((i % 3) == 0){
				System.out.print(i + " ");
				i = i+3;

			} else {
				i++;
			}
		}
	}	

}