/**
* @author Victoria Gorskaya
* 11-801
* Task 12 
*/
import java.util.*;
import java.lang.Math.*;

public class Task12{
	public static void main(String[] args){
		int [] a = {958, 232, 322, 34, 185, 233, 75431, 111211};
		int i = 1;
		int n = a.length;
		boolean flag = false;
		
		while(i < n-1 && !flag){
			if (a[i] > a[i-1] && a[i]>a[i+1] && (a[i] % 2 == 0)){
				flag = true;
			} 
			i++;
		}

		System.out.println(flag);
	
	}

}