/**
* @author Victoria Gorskaya
* 11-801
* Task 05 
*/
import java.util.*;
public class Task05{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		double sum = 1.0;
		double m = 1.0;
		int x = sc.nextInt();
		int i =1;

		while (((m*x)/i) > 01e-9) {
			m = (m*x) / i;
			sum = sum + m;
			i++;
		}
		System.out.println(sum);
	}	

}