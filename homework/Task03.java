/**
* @author Victoria Gorskaya
* 11-801
* Task 03 
*/
import java.util.*;
import java.lang.Math.*;

public class Task03{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int x = sc.nextInt();
		double n = x;
		double i = 0;
		double k;
		while ((n-i)> 1e-9) {
		k = (n + i)/2;
			if (k*k > x) {
				n = k;
			} else {
				i = k;
			}
		}

	int sqr = (int) n;		
	System.out.println(sqr);

	}			


}