/**
* @author Victoria Gorskaya
* 11-801
* Task 04 
*/
import java.util.*;
import java.lang.Math.*;

public class Task04{
public static void main(String[] args) { 
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int i;
    int j;
    for (i = -n; i <= n; i++) {
        for (j = -n; j <= n; j++) {
            if (i*i + j*j <= n*n){
                    System.out.print("0");
                }
            else{
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}