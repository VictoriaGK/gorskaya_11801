package ru.twitter.classesOfTwitter;

import java.util.*;

public class Comment extends Tweet {
	private int idTweetOfComment;
	private int idComment;
	private int idProfileOfComment;

	public Comment(int tweetID, int profileID, String dateOfPost, String timeOfPost, int idOfText, ArrayList<Comment> comments, int idTweetOfComment, int idComment, int idProfileOfComment){
		super(tweetID, profileID, dateOfPost, timeOfPost, idOfText, comments);
		this.idTweetOfComment = idTweetOfComment;
		this.idComment = idComment;
		this.idProfileOfComment = idProfileOfComment;
	}

	public int getIdTweetOfComment(){
		return idTweetOfComment;
	}

	public int getIdComment() {
		return idComment;
	}

	@Override
	public int getProfileID() {
		return super.getProfileID();
	}

	@Override
	public String toString() {

		return "Comment {" +
				"commentID = " + idComment +
				" }";
	}
}