package ru.twitter.classesOfTwitter;

import java.util.*;

public class Profile {
    private int profileID;
    private String name;
    private String born;
    private String joined;
    private ArrayList<Tweet> tweets;
    private ArrayList<Profile> followings;
    private ArrayList<Profile> followers;
    private ArrayList<Comment> comments;
    private ArrayList<Repost> reposts;

    public Profile(int profileID, String name, String born, String joined, ArrayList<Tweet> tweets,
                   ArrayList<Profile> followings, ArrayList<Profile> followers, ArrayList<Comment> comments, ArrayList<Repost> reposts) {
        this.profileID = profileID;
        this.name = name;
        this.born = born;
        this.joined = joined;
        this.tweets = tweets;
        this.followings = followings;
        this.followers = followers;
        this.comments = comments;
        this.reposts = reposts;
    }

    public void addFollower(Profile follower) {
        this.followers.add(follower);
    }

    public void addFollowing(Profile following) {
        this.followings.add(following);
    }

    public int getProfileID() {
        return profileID;
    }

    public String getName(){return name;}

    public ArrayList<String> toStringList() {

        ArrayList<String> s = new ArrayList<>();
        s.add( "Profile {" +
                "profileID = " + profileID +
                ", name = " + name +
                ", born = '" + born + '\'' +
                ", joined = '" + joined + '\'' +
                " }");

        for (int i = 0; i < tweets.size(); i++) {
            s.add("   " + tweets.get(i).toString());
        }
        for (int i = 0; i < followers.size(); i++) {
            s.add("   " + followers.get(i).toString());
        }
        for (int i = 0; i < followings.size(); i++) {
            s.add("   " + followings.get(i).toString());
        }
        for (int i = 0; i < comments.size(); i++) {
            s.add("   " + comments.get(i).toString());
        }
        for (int i = 0; i < reposts.size(); i++) {
            s.add("   " + reposts.get(i).toString());
        }

        return s;
    }
}
