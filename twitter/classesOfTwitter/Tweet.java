package ru.twitter.classesOfTwitter;

import ru.twitter.classesOfTwitter.Comment;

import java.util.*;
public class Tweet {

    protected int tweetID;
    protected int profileID;
    protected String date;
    protected String time;
    protected int idOfText;
    protected ArrayList<Comment> comments;

    public Tweet(int tweetID, int profileID, String dateOfPost, String timeOfPost, int idOfText, ArrayList<Comment> comments) {
        this.profileID = profileID;
        this.date = dateOfPost;
        this.time = timeOfPost;
        this.idOfText = idOfText;
        this.tweetID = tweetID;
        this.comments = comments;
    }
    public int getTweetID() { return tweetID; }


    public int getProfileID() {
        return profileID;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public int getIdTextext() {
        return idOfText;
    }

    @Override
    public String toString() {
        return "Tweet{" +
                "tweetID=" + tweetID +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", idOfText='" + idOfText + '\'' +
                '}';
    }
}
