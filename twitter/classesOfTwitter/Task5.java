package ru.twitter.classesOfTwitter;

import java.util.ArrayList;

public class Task5 {
    public static boolean p(Profile pr, ArrayList<Tweet> tweets, ArrayList<Comment> comments){
        return numComments(pr, comments) >= numTweets(pr, tweets);

    }
    public static int numComments(Profile pr, ArrayList<Comment> comments){
        int k = 0;
        for(Comment comment: comments){
           if (comment.getProfileID() == pr.getProfileID())
               k++;
        }
        return  k;
    }
    public static int numTweets(Profile pr, ArrayList<Tweet> tweets){
        int k = 0;
        for(Tweet tweet: tweets){
            if (tweet.getProfileID() == pr.getProfileID())
                k++;
        }
        return  k;
    }
}
