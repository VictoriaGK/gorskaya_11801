package ru.itis.kpfu;

public class Comment {
    private String comment;
    private Post post;
    private User whoPosted;

    public Comment(String comment, Post post, User whoPosted) {
        this.comment = comment;
        this.post = post;
        this.whoPosted = whoPosted;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getWhoPosted() {
        return whoPosted;
    }

    public void setWhoPosted(User whoPosted) {
        this.whoPosted = whoPosted;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "comment='" + comment + '\'' +
                ", post=" + post +
                ", whoPosted=" + whoPosted +
                '}';
    }
}
