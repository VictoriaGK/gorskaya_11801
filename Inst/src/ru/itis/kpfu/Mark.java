package ru.itis.kpfu;

public class Mark {
    private Post post;
    private User user;

    public Mark() {
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Mark(Post post, User user) {
        this.post = post;
        this.user = user;
    }

    @Override
    public String toString() {
        return "Mark{" +
                "post=" + post +
                ", user=" + user +
                '}';
    }
}
