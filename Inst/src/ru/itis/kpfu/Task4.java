package ru.itis.kpfu;

import java.util.ArrayList;

public class Task4 {
    public static boolean p(User user, ArrayList<Post> posts, ArrayList<Following> followings){

        for (Following following: followings){
            if(following.getFollowing().getUsername().equals(user.getUsername())){
                if(numPost(following.getFollower(),posts)>= numPost(user, posts)){
                    return true;
                }
            }
        }
        return false;
    }
    public static int numPost(User user, ArrayList<Post> posts){
        int k = 0;
        for(Post post: posts){
            if(post.getWhoUpload().equals(user))
                k++;
        }
        return k;
    }
}
