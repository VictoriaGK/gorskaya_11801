package ru.itis.kpfu;

import java.util.ArrayList;

public class User {
    private String name;
    private String pass;
    private String website;
    private String username;
    private String bio;
    private String email;
    private String phone;
    private boolean gender;
    private String profilePhoto;

    public User(String name, String pass, String website, String username, String bio, String email, String phone, boolean gender, String profilePhoto) {
        this.name = name;
        this.pass = pass;
        this.website = website;
        this.username = username;
        this.bio = bio;
        this.email = email;
        this.phone = phone;
        this.gender = gender;
        this.profilePhoto = profilePhoto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", pass='" + pass + '\'' +
                ", website='" + website + '\'' +
                ", username='" + username + '\'' +
                ", bio='" + bio + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", gender=" + (gender? "male": "female") +
                ", profilePhoto='" + profilePhoto + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public int countPosts(ArrayList<Post> posts) {
        int count = 0;
        for (Post post: posts) {
            if (post.getWhoUpload() == this) {
                count++;
            }
        }
        return count;
    }

}
