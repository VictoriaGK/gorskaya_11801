//Вариант 2
public class Task01 {
    public static int[] task01(int[] mas){
        int max = -1;
        int min = mas[0];
        int k;
        for (int i = 0; i < mas.length; i++) {
            if (mas[i] < min)
                min = mas[i];

            if (mas[i] > max)
                max = mas[i];
        }

        if (max >= mas.length) max = mas.length-1;

        int j= max;

        for (int i = min; i <= ((max+min)/2); i++) {
            k = mas[i];
            mas[i] = mas[j];
            mas[j] = k;
            j--;

        }return mas;

    }
    public static void main(String[] args) {
        int [] s = {6,5,3,4,5,6,7,8};
        int [] s1 = Task01.task01(s);
            for (int i = 0; i <s1.length ; i++) {
                System.out.println(s1[i]);

            }
    }

}