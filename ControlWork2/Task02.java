//Вар2
public class Task02 {
    public static boolean task02(char[][] chars, String[] strings){
        boolean isExist = false;
        int k = 0;
        int index;
        while (!isExist && k < strings.length-1 ){
            for (int i = 0; i < chars.length; i++) {
                if (isFindWord(strings[k], chars[i])){
                    index = i;
                    for (int j = 0; j < chars.length; j++) {
                        if (isFindWord(strings[k + 1], chars[j])) {
                            if(index < j ) {
                                isExist = true;
                            }
                        }
                    }
                }
            }
            k++;

        }return isExist;

    }
    public static boolean isFindWord(String string, char[] chars){
        int j = 0;
        for (int i = 0; i < chars.length; i++){
            while ((j<string.length())&&(string.charAt(j) == chars[i])){
                j++; i++;
            }
            if (j == string.length()){
                return true;
            }else j = 0;

        }return false;

    }



    public static void main(String[] args) {

        String[] str = {"nana","mama"};
        char[][] chr = {{'n','a','n','a'},
                {'m','a','m','a'},
                {'n','d','b','a'},
        };
        System.out.println(Task02.task02(chr,str));
    }
}