package ru.itis.kpfu;

import java.util.*;

public class Task2{


    public Map<String, Integer> countMovie(ArrayList<Movie> movies, ArrayList<Participation> participations){
        int count = 1;
        Map<String, Integer> countPA = new HashMap<>();
        for (Participation participation: participations) {
            for (Movie movie: movies) {
                if(participation.getIdMovie()==movie.idMovie){

                    String s = " " + participation.getIdActer() + " " + movie.getIdProd();
                    if (countPA.isEmpty()){
                        countPA.put(s, count);
                    }else{

                        for (Map.Entry<String, Integer> pair: countPA.entrySet())
                        {
                            if(countPA.containsValue(s)){
                                count = pair.getValue() + 1;
                                pair.setValue(count);
                            }
                            else
                            {
                                count = 1;
                                countPA.put(s, count);
                            }
                        }


                    }


                }

            }



        }
        return countPA;
    }
}
