package ru.itis.kpfu;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Task3 {
    public ArrayList<Acter> acters(ArrayList<Acter> acters,ArrayList<Participation> participations,ArrayList<Movie> movies, int x) {
        return acters.stream().filter(y ->oneProd(y,participations, movies, x)).collect(Collectors.toCollection(() -> new ArrayList<>()));
    }

    public ArrayList<Acter> oneProdAct(ArrayList<Acter> acters,ArrayList<Participation> participations,ArrayList<Movie> movies, int x){
        ArrayList<Acter> acters1 = new ArrayList<>();
        for (Acter acter: acters) {
            if(oneProd(acter, participations, movies, x)){
                acters1.add(acter);
            }


        }

        return acters1;
    }
    public boolean oneProd(Acter acter, ArrayList<Participation> participations,ArrayList<Movie> movies, int x){
        int idProd = 0;
        for (Participation participation: participations) {
            if( participation.getIdActer()== acter.getId()){
                for (Movie movie: movies) {
                    if(participation.getIdMovie()==movie.getIdMovie() && movie.getYear()>x){
                        if(!(idProd == 0 || movie.getIdProd()==idProd)){
                            return false;
                        }
                        else idProd = movie.getIdProd();
                    }

                 }

            }

        }
        return true;
    }
}
