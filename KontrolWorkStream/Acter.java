package ru.itis.kpfu;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Acter {
    String name;
    int id;
    int seniority;

    public Acter(String name, int id, int seniority) {
        this.name = name;
        this.id = id;
        this.seniority = seniority;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getSeniority() {
        return seniority;
    }



}
