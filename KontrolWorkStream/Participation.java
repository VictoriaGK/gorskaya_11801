package ru.itis.kpfu;

public class Participation {
    int idMovie;
    int idActer;
    String namePerson;


    public Participation(int idMovie, int idActer, String namePerson) {
        this.idMovie = idMovie;
        this.idActer = idActer;
        this.namePerson = namePerson;
    }

    public int getIdMovie() {
        return idMovie;
    }

    public int getIdActer() {
        return idActer;
    }

    public String getNamePerson() {
        return namePerson;
    }
}
