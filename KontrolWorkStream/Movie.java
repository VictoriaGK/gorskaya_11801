package ru.itis.kpfu;

public class Movie {
    String name;
    int year;
    int idProd;
    int idMovie;
    public Movie(String name, int year, int idProd, int idMovie) {
        this.name = name;
        this.year = year;
        this.idProd = idProd;
        this.idMovie = idMovie;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public int getIdProd() {
        return idProd;
    }

    public int getIdMovie() {
        return idMovie;
    }
}
