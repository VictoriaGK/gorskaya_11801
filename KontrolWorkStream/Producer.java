package ru.itis.kpfu;

import java.util.ArrayList;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Producer {
String FIO;
int id;

    public Producer(String FIO, int id) {
        this.FIO = FIO;
        this.id = id;
    }

    public String getFIO() {
        return FIO;
    }

    public int getId() {
        return id;
    }

    public ArrayList<Movie> listMovie(ArrayList<Movie> movies, int year) {
        return movies.stream().filter(y -> y.getIdProd() == this.getId() && y.getYear()<year).collect(Collectors.toCollection(() -> new ArrayList<>()));
    }


}
