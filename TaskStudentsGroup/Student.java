import java.io.*;
import java.util.*;
import java.io.*;
import java.util.*;
import java.util.*;
import java.lang.*;
public class Student{
	
	String name;
	String city;
	int year;
	String group;
	public static Student[] students;
	private static Student[] get() throws IOException {
		
		if (students == null){	
			Scanner scan = new Scanner(new File("reps.txt"));
			int n = Integer.parseInt(scan.nextLine());
			Student[] students = new Student[n];
			
			for  (int i = 0; i< n; i++) {
				String line = scan.nextLine();
				String [] lst = line.split("\t");
				students[i] = new Student(lst[0],lst[1],Integer.parseInt(lst[2]),lst[3]);	
			}
		}
		
		return students;
	}


	public Student(String name,String city, int year, String group){
		this.name = name;
		this.city = city;
		this.year = year;
		this.group = group;

	}
	public Student(){

	}
	public String getGroup(){
		return group;
	}
	public String getName(){
		return name;
	}
	public String getCity(){
		return city;
	}
	public int getYear(){
		return year;
	}
	public String toString(){
		return "( " + name+ " " + city +" " + year +" " + group +")"; 
	}
	
	

	public  void getStdGrp(String group) throws IOException{
		Student[] students = Student.get();
		
		
		for (int i = 0; i <students.length; i++){

			if ((students[i]).getGroup().equals(group)){
				System.out.println(students[i].getName());
			}
		}
	}	
		
	public void getStdKzn1() throws IOException{
		String city = "Казань";
		Group[] groups = Group.get();
		Student[] students = Student.get();
		int course =1;
		for (int i = 0; i <students.length; i++){

			if (students[i].getCity().equals(city)){
				group = students[i].getGroup();

				for (int j = 0; j < groups.length; j++){

					if (((groups[j]).getGroup().equals(group))&&(groups[j].getCourse() == course)){
						System.out.println(students[i].getName());

					}
				}

			}

		}

	}
	public boolean getGrpCourse(String group, int course) throws IOException{ 
		Group[] groups = Group.get();
		for (int i = 0; i <groups.length; i++){
			if (((groups[i]).getGroup().equals(group))&&(groups[i].getCourse() == course)){
				return true;

			}
		}return false;

	}
	public void getStd18Kzn2(int yearToday) throws IOException{
		Group[] groups = Group.get();
		Student[] students = Student.get();

		for (int i = 0; i< students.length; i++){

			if (((yearToday-(students[i].getYear()))>=18)&&(students[i].getCity().equals(city))&&(students[i].getGrpCourse(students[i].getGroup(), 2))){
				System.out.println(students[i].getName());

			}
		}


	}

}