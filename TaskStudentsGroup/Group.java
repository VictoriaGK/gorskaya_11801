import java.io.*;
import java.util.*;
import java.util.*;
import java.lang.*;
public class Group{
	String group;
	int course;
	int countStudent;
	public Group(){
	};
	public Group(String group, int course){
		this.group = group;
		this.course = course;
	}
	public String getGroup(){
		return group;
	}
	public int getCourse(){
		return course;
	}
	public String toString(){
		return "( " + group + " "  + course +" )"; 
	}


	public static Group[] get() throws IOException {
		
		Scanner scan = new Scanner(new File("reps2.txt"));
		int n = Integer.parseInt(scan.nextLine());
		Group[] groups = new Group[n];

		
		for (int i = 0; i < n; i++){
			String line = scan.nextLine();
			String [] lst = line.split("\t");
			groups[i] = new Group(lst[0],Integer.parseInt(lst[1]));
		}

	return groups;

	}


	public void getCountStdGrp() throws IOException{ 
		Group[] groups = Group.get();
		Student[] students = Student.get();
		
		for (int i = 0; i <groups.length; i++){
			int k = 0;
			String group = groups[i].getGroup();
			for (int j = 0; j <students.length; j++){

				if ((students[j]).getGroup().equals(group)){
					k++;
				}
			
		}
		System.out.println(k + "  " + groups[i].getGroup());

		}
	}


	public  void getStudСourse(int course) throws IOException{
		Group[] groups = Group.get();
		Student[] students = Student.get();
		
		for (int i = 0; i <groups.length; i++){

			if ((groups[i]).getCourse() == course){
				students[0].getStdGrp(groups[i].getGroup());
						//System.out.println(groups[i].getCourse());

			}
		}

	}
}