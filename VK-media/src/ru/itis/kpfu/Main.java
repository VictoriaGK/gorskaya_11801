package ru.itis.kpfu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static ArrayList<Category> categories = new ArrayList<>();
    public static ArrayList<Genre> genres = new ArrayList<>();
    public static ArrayList<Game> games = new ArrayList<>();
    public static ArrayList<Albom> alboms = new ArrayList<>();
    public static ArrayList<Photo> photos = new ArrayList<>();
    public static ArrayList<AudioAuthor> audioAuthors = new ArrayList<>();
    public static ArrayList<User> users = new ArrayList<>();
    public static ArrayList<Audio> audios = new ArrayList<>();
    public static  ArrayList<Goods> goods = new ArrayList<>();
    public static ArrayList<Video> videos = new ArrayList<>();
    public static ArrayList<UsersAudio> usersAudios = new ArrayList<>();



    public static String findAutorById(int id, ArrayList<AudioAuthor> artists) {
        for (int i = 0; i < artists.size(); i++) {
            if (artists.get(i).getId() == id) {
                return artists.get(i).getAutor();
            }
        }
        return null;
    }
    public static String findUserById(int id, ArrayList<User> users) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == id) {
                return users.get(i).getUserName();
            }
        }
        return null;

    }
    public static String findAlbomById (int id, ArrayList<Albom> alboms) {
        for (int i = 0 ; i < alboms.size(); i++) {
            if ( alboms.get(i).getId() == id) {
                return alboms.get(i).getTitle();
            }
        }
        return null;
    }
    public static  String findAudioById (int id , ArrayList<Audio> audios) {
        for (int i = 0; i < audios.size(); i++) {
            if (audios.get(i).getId() == id) {
                return audios.get(i).getSong();
            }
        }
        return null;
    }


    public static String findCategoryById (int id, ArrayList<Category> categories) {
        for (int i = 0 ; i < categories.size(); i++) {
            if (categories.get(i).getId() ==  id) {
                return categories.get(i).getTitle();
            }
        }
        return null;
    }

    public static String findGenreById (int id, ArrayList<Genre> genres) {
        for (int i = 0 ; i < genres.size(); i++) {
            if (genres.get(i).getId() == id) {
                return genres.get(i).getTitle();
            }
        }
        return null;
    }

    public static void getCategories() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Category.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Category cg = new Category(Integer.parseInt(data[0]), data[1]);
            categories.add(cg);
        }
    }

    public static void getGenres() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Genres.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Genre genre = new Genre(Integer.parseInt(data[0]), data[1]);
            genres.add(genre);
        }
    }

    public static ArrayList<Game> getGames() throws FileNotFoundException {
        Scanner sc= new Scanner(new File("Games.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Game g = new Game(data[0], Integer.parseInt(data[1]), findGenreById(Integer.parseInt(data[2]),genres), findCategoryById(Integer.parseInt(data[3]),categories), data[4]);
            games.add(g);
        }
        return games;
    }

    public static void getAlboms() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Alboms.txt"));

        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Albom albom = new Albom(Integer.parseInt(data[0]),data[1]);
            alboms.add(albom);
        }
    }

    public static ArrayList<Photo> getPhotos() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Photo.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Photo p = new Photo(findAlbomById(Integer.parseInt(data[0]), alboms), Integer.parseInt(data[1]), Integer.parseInt(data[2]), data[3], data[4]);
            photos.add(p);
        }
        return photos;
    }

    public static void getAudioAuthors() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Autors.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            AudioAuthor au = new AudioAuthor(Integer.parseInt(data[0]), data[1]);
            audioAuthors.add(au);
        }
    }

    public static ArrayList<Audio> getAudios() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Audio.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Audio a = new Audio(Integer.parseInt(data[0]),findAutorById(Integer.parseInt(data[1]),
                    audioAuthors), data[2], Short.parseShort(data[3]),
                    Integer.parseInt(data[4]), data[5]);
            audios.add(a);
        }
        return audios;
    }

    public static ArrayList<UsersAudio> getUsersAudio() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("UsersAudio.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split(("\t"));
            UsersAudio ua = new UsersAudio(findUserById(Integer.parseInt(data[0]), users),
                    findAudioById(Integer.parseInt(data[1]), audios));
            usersAudios.add(ua);
        }
        return usersAudios;
    }

    public static void getUsers() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Users.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            User user = new User(Integer.parseInt(data[0]), data[1]);
            users.add(user);
        }
    }

    public static ArrayList<Goods> getGoods() throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Goods.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Goods good = new Goods(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], data[6], findUserById(Integer.parseInt(data[7]),users));
            goods.add(good);
        }
        return goods;
    }

    public static ArrayList<Video> getVideos() throws FileNotFoundException {
        Scanner sc = new Scanner (new File("Video.txt"));
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Video v = new Video(Integer.parseInt(data[0]), Integer.parseInt(data[1]), Short.parseShort(data[2]), findUserById(Integer.parseInt(data[3]),users),data[4],Integer.parseInt(data[5]),Integer.parseInt(data[6]));
            videos.add(v);
        }
        return videos;
    }



    public static void main(String[] args) throws IOException {
//        getCategories();
//        getGenres();
//        System.out.println(getGames());
//        getAlboms();
//        System.out.println(getPhotos());
//        getAudioAuthors();
//        System.out.println(getAudios());
//        getUsers();
//        System.out.println(getGoods());
//        System.out.println(getVideos());

    for(User user: users){
        if(Task2.allAudio(user,"Wham!",audios, usersAudios)){
            System.out.println(user.getUserName());
        }

    }


    }




}
