package ru.itis.kpfu;

import java.util.ArrayList;

public class Task2 {
    public static boolean allAudio(User user, String author, ArrayList<Audio> audios, ArrayList<UsersAudio> usersAudios) {
        for (Audio audio : audios) {
            if (audio.getAuthor().equals(author)) {
                if (!(usersAudioExist(user, audio, usersAudios))) {
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean usersAudioExist(User user, Audio audio, ArrayList<UsersAudio> usersAudios){
        for(UsersAudio usersAudio: usersAudios) {
            if ((Integer.parseInt(usersAudio.getAudio()) == (audio.getId()))&&(Integer.parseInt(usersAudio.getUser())==(user.getId()))) {
                    return true;
                }
            }
        return false;
    }
}
