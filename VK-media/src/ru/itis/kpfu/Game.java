package ru.itis.kpfu;

public class Game {
    private String name;
    private int rating;
    private String genre;
    private  String category;
    private String icon;


    public Game(String name, int rating, String genre, String category, String icon) {
        this.name = name;
        this.rating = rating;
        this.genre = genre;
        this.category = category;
        this.icon = icon;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(byte rating) {
        this.rating = rating;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String toString() {
        return name + " " +  rating + " " + genre + " " + category + " " + icon + " " ;
    }


}
