//package ru.itis.kpfu;
/*import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class MainClass {






    public static String findAutorById(int id, ArrayList<AudioAuthor> artists) {
        for (int i = 0; i < artists.size(); i++) {
            if (artists.get(i).getId() == id) {
                return artists.get(i).getAutor();
            }
        }
        return null;
    }
    public static String findUserById(int id, ArrayList<User> users) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == id) {
                return users.get(i).getUserName();
            }
        }
        return null;

    }

    public static String findAlbomById (int id, ArrayList<Albom> alboms) {
        for (int i = 0 ; i < alboms.size(); i++) {
            if ( alboms.get(i).getId() == id) {
                return alboms.get(i).getTitle();
            }
        }
        return null;
    }


    public static String findCategoryById (int id, ArrayList<Category> categories) {
        for (int i = 0 ; i < categories.size(); i++) {
            if (categories.get(i).getId() ==  id) {
                return categories.get(i).getTitle();
            }
        }
        return null;
    }

    public static String findGenreById (int id, ArrayList<Genre> genres) {
        for (int i = 0 ; i < genres.size(); i++) {
            if (genres.get(i).getId() == id) {
                return genres.get(i).getTitle();
            }
        }
        return null;
    }

    public static void main (String[] args)
    throws IOException {

        Scanner sc5 = new Scanner(new File("Category.txt"));
        ArrayList<Category> categories = new ArrayList<>();

        //Cчитывание category.txt
        while (sc5.hasNextLine()) {
            String[] data = sc5.nextLine().split("\t");
            Category cg = new Category(Integer.parseInt(data[0]), data[1]);
            categories.add(cg);
        }

        Scanner sc6 = new Scanner(new File("Genres.txt"));
        ArrayList<Genre> genres = new ArrayList<>();

        while (sc6.hasNextLine()) {
            String[] data = sc6.nextLine().split("\t");
            Genre genre = new Genre(Integer.parseInt(data[0]), data[1]);
            genres.add(genre);
        }


        Scanner sc0= new Scanner(new File("Games.txt"));
        ArrayList<Game> games = new ArrayList<>();
        //Games.txt
        while (sc0.hasNextLine()) {
            String[] data = sc0.nextLine().split("\t");
            Game g = new Game(data[0], Integer.parseInt(data[1]), findGenreById(Integer.parseInt(data[2]),genres), findCategoryById(Integer.parseInt(data[3]),categories), data[4]);
            games.add(g);
        }
        System.out.println(games);

        Scanner sc7 = new Scanner(new File("Alboms.txt"));
        ArrayList<Albom> alboms = new ArrayList<>();

        while (sc7.hasNextLine()) {
            String[] data = sc7.nextLine().split("\t");
            Albom albom = new Albom(Integer.parseInt(data[0]),data[1]);
            alboms.add(albom);
        }


        Scanner sc = new Scanner(new File("Photo.txt"));
        ArrayList<Photo> photos = new ArrayList<>();
        //Photo
        while (sc.hasNextLine()) {
            String[] data = sc.nextLine().split("\t");
            Photo p = new Photo(findAlbomById(Integer.parseInt(data[0]), alboms), Integer.parseInt(data[1]), Integer.parseInt(data[2]), data[3], data[4]);
            photos.add(p);
        }
        System.out.println(photos);


        //Считывание AudioAutors
        Scanner sc2 = new Scanner(new File("Autors.txt"));
        ArrayList<AudioAuthor> audioAuthors = new ArrayList<>();
        while (sc2.hasNextLine()) {
            String[] data = sc2.nextLine().split("\t");
            AudioAuthor au = new AudioAuthor(Integer.parseInt(data[0]), data[1]);
            audioAuthors.add(au);
        }

        //Считывание Users
        Scanner sc1 = new Scanner(new File("Users.txt"));
        ArrayList<User> users = new ArrayList<>();
        while (sc1.hasNextLine()) {
            String[] data = sc1.nextLine().split("\t");
            User user = new User(Integer.parseInt(data[0]), data[1]);
            users.add(user);
        }

        //Считывание Audio
        Scanner sc3 = new Scanner(new File("Audio.txt"));
        ArrayList<Audio> audios = new ArrayList<>();

        while (sc3.hasNextLine()) {
            String[] data = sc3.nextLine().split("\t");
            Audio a = new Audio(findAutorById(Integer.parseInt(data[0]), audioAuthors), data[1], Short.parseShort(data[2]), Integer.parseInt(data[3]), data[4], data[5]);
            audios.add(a);
        }


        Scanner sc4 = new Scanner(new File("Goods.txt"));
        ArrayList<Goods> goods = new ArrayList<>();

        while (sc4.hasNextLine()) {
            String[] data = sc4.nextLine().split("\t");
            Goods good = new Goods(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]), data[3], data[4], data[5], data[6], findUserById(Integer.parseInt(data[7]),users));
            goods.add(good);
        }

        Scanner scan1 = new Scanner (new File("Video.txt"));
        ArrayList<Video> videos = new ArrayList<>();

        while (scan1.hasNextLine()) {
            String[] data = scan1.nextLine().split("\t");
            Video v = new Video(Integer.parseInt(data[0]), Integer.parseInt(data[1]), Short.parseShort(data[2]), findUserById(Integer.parseInt(data[3]),users),data[4],Integer.parseInt(data[5]),Integer.parseInt(data[6]));
            videos.add(v);
        }
        System.out.println(videos);

        System.out.println(audios);
        System.out.println(goods);





    }
}*/
