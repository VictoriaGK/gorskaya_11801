package ru.itis.kpfu;

public class Audio {
    private int id ;
    private String author;
    private String song;
    private short quality;
    private int timeInSec;
    private String lyrics;

    public Audio(int id, String author, String song, short quality, int timeInSec, String lyrics) {
        this.id = id;
        this.author = author;
        this.song = song;
        this.quality = quality;
        this.timeInSec = timeInSec;
        this.lyrics = lyrics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public short getQuality() {
        return quality;
    }

    public void setQuality(short quality) {
        this.quality = quality;
    }

    public int getTimeInSec() {
        return timeInSec;
    }

    public void setTimeInSec(int timeInSec) {
        this.timeInSec = timeInSec;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    public String toString() {
        return author + " " + song + " " + quality + " " + timeInSec + " " + lyrics;
    }
}

