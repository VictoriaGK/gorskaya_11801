package ru.itis.kpfu;

public class Goods {
    private String name;
    private int likes;
    private int reposts;
    private String description;
    private String timeAdded;
    private String price;
    private String place;
    private String whoAdded;

    public Goods(String name, int likes, int reposts, String description, String timeAdded, String price, String place, String whoAdded) {
        this.name = name;
        this.likes = likes;
        this.reposts = reposts;
        this.description = description;
        this.timeAdded = timeAdded;
        this.price = price;
        this.place = place;
        this.whoAdded = whoAdded;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getReposts() {
        return reposts;
    }

    public void setReposts(int reposts) {
        this.reposts = reposts;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getDescription() {
        return description;
    }


    public String getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(String timeAdded) {
        this.timeAdded = timeAdded;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getWhoAdded() {
        return whoAdded;
    }

    public void setWhoAdded(String whoAdded) {
        this.whoAdded = whoAdded;
    }

    public String toString() {
        return name + " " + likes + " " + reposts + " " + description + " " + timeAdded + " " + price + " " + place + " " + whoAdded;
    }
}
