package ru.itis.kpfu;

public class Photo {
    private String albom;
    private int likes;
    private int repost;
    private String description;
    private String timeAdded;


    public Photo(String albom, int likes, int repost, String description, String timeAdded) {
        this.albom = albom;
        this.likes = likes;
        this.repost = repost;
        this.description = description;
        this.timeAdded = timeAdded;
    }

    public String getAlbom() {
        return albom;
    }

    public void setAlbom(String albom) {
        this.albom = albom;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getRepost() {
        return repost;
    }

    public void setRepost(int repost) {
        this.repost = repost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(String timeAdded) {
        this.timeAdded = timeAdded;
    }

    @Override
    public String toString() {
        return albom + " " + likes + " "  + repost + " "  + description + " " + timeAdded ;
    }
}
