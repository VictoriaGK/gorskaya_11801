import java.util.*;
public class Task4 {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		int digit;  
		int countDigit = 0;
		while (n > 0){
			n = n / 10;
			countDigit = countDigit + 1;
		} 
		System.out.println(countDigit);
	}

}