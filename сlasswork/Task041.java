import java.util.*;
import java.lang.Math.*;

public class Task041{
	public static void main(String[] args){
		int [] a = {1, 5, 5 , 4, 5};
		int k = 1;
		int i = 0;
		int n = a.length;
		int count = 0;
		boolean flag = true;
		while(flag && i < n){

			if (a[i] % 5 == 0){
				count++;
			}

			if (k < count){
				flag = false;
			}
			i++;
		} 

		if(count != k){
			flag = false;
		}

		System.out.print(flag);

	}	

}