import java.util.*;
public class Task5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int notDigit = sc.nextInt();
		int newNumber = 0;
		int tenNumber = 1;
		while (n > 0){
			if (n % 10 != notDigit){
				newNumber = (n % 10) * tenNumber + newNumber;
				tenNumber = 10 * tenNumber; 
			}
			n = n / 10;
		}

		System.out.println(newNumber);
	}

}