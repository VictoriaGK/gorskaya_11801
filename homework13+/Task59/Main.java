import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("Browser_Speed.csv"));
        ArrayList<Browser_Speed> browser_speeds = new ArrayList<Browser_Speed>();
        sc.nextLine();
        sc.nextLine();
        while (sc.hasNextLine()){
            String[] s = sc.nextLine().split(",");
            Browser_Speed browser_speed = new Browser_Speed(s[0], Double.parseDouble(s[1]), Double.parseDouble(s[3]), Double.parseDouble(s[4]),
                    Double.parseDouble(s[5]),Double.parseDouble(s[6]),Double.parseDouble(s[7]),Double.parseDouble(s[8]),Double.parseDouble(s[9]),
                    Double.parseDouble(s[10]),Double.parseDouble(s[11]));
            browser_speeds.add(browser_speed);

        }

        for(Browser_Speed browser_speed: browser_speeds) {
            System.out.println(browser_speed);
            System.out.println(averageSpeedAmazon(browser_speeds));
            System.out.println(averageSpeedBrowser(browser_speeds));
        }

    }
    public static String averageSpeedBrowser(ArrayList<Browser_Speed> browser_speeds){
        double all = 0;
        double min = 1000000;
        Browser_Speed minBrowser = null;
        for(Browser_Speed browser_speed: browser_speeds){
           if(average(browser_speed) < min) {
               min = average(browser_speed);
               minBrowser = browser_speed;
           }

        }return minBrowser.getBrowser();
    }

    public static double average(Browser_Speed browser_speed){
        return browser_speed.getYoutube() + browser_speed.getYahoo() + browser_speed.getWikipedia() + browser_speed.getPcworld()
                + browser_speed.getMyspace() + browser_speed.getMicrosoft() + browser_speed.getEbay() + browser_speed.getApple()
                + browser_speed.getAmazon();
    }





    public  static double averageSpeedAmazon(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getAmazon();
            count++;
        }return sum/count;
    }
    public double averageSpeedApple(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getApple();
            count++;
        }return sum/count;
    }
    public double averageSpeedEbay(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getEbay();
            count++;
        }return sum/count;
    }
    public double averageSpeedMicrosoft(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getMicrosoft();
            count++;
        }return sum/count;
    }
    public double averageSpeedMyspace(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getMyspace();
            count++;
        }return sum/count;
    }
    public double averageSpeedPCWorld(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getPcworld();
            count++;
        }return sum/count;
    }
    public double averageSpeedWikipedia(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getWikipedia();
            count++;
        }return sum/count;
    }
    public double averageSpeedYahoo(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getYahoo();
            count++;
        }return sum/count;
    }
    public double averageSpeedYouTube(ArrayList<Browser_Speed> browser_speeds){
        double sum = 0;
        int count = 0;
        for (Browser_Speed browser_speed: browser_speeds){
            sum = sum + browser_speed.getYoutube();
            count++;
        }return sum/count;
    }





}