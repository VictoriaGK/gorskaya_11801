import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Browser_Speed {
    private String browser;
    private double average;
    private double amazon;
    private double apple;
    private double ebay;
    private double microsoft;
    private double myspace;
    private double pcworld;
    private double wikipedia;
    private double yahoo;
    private double youtube;

    public Browser_Speed(String browser, double average, double amazon, double apple, double ebay, double microsoft,
                         double myspace, double pcworld, double wikipedia, double yahoo, double youtube) {
        this.browser = browser;
        this.average = average;
        this.amazon = amazon;
        this.apple = apple;
        this.ebay = ebay;
        this.microsoft = microsoft;
        this.myspace = myspace;
        this.pcworld = pcworld;
        this.wikipedia = wikipedia;
        this.yahoo = yahoo;
        this.youtube = youtube;
    }

    @Override
    public String toString() {
        return "Browser_Speed{" +
                "browser='" + browser + '\'' +
                ", average=" + average +
                ", amazon=" + amazon +
                ", apple=" + apple +
                ", ebay=" + ebay +
                ", microsoft=" + microsoft +
                ", myspace=" + myspace +
                ", pcworld=" + pcworld +
                ", wikipedia=" + wikipedia +
                ", yahoo=" + yahoo +
                ", youtube=" + youtube +
                '}';
    }

    public String getBrowser() {
        return browser;
    }


    public double getAverage() {
        return average;
    }

    public double getAmazon() {
        return amazon;
    }

    public double getApple() {
        return apple;
    }

    public double getEbay() {
        return ebay;
    }

    public double getMicrosoft() {
        return microsoft;
    }

    public double getMyspace() {
        return myspace;
    }

    public double getPcworld() {
        return pcworld;
    }

    public double getWikipedia() {
        return wikipedia;
    }

    public double getYahoo() {
        return yahoo;
    }

    public double getYoutube() {
        return youtube;
    }
}
