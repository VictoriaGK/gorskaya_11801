/**
* @author Victoria Gorskaya
* 11-801
* Task 19 
*/
package ru.itis.kpfu;

public class Task19 {
    

   public static  long[] task19(int[] digit, int dig){
       long[] result = new long[(digit.length+1)];

        for (int i = 0; i < digit.length; i++) {
                result[i] = digit[i] * dig;

        }


        while(!allLessTen(result)){
            if (result[0]>9){
                long[] res = new long[result.length+1];
                for (int i = 1; i <res.length ; i++) {
                    res[i] = result[i-1];

                }
                result = res;
            }
            for (int i = 1; i < result.length; i++) {
                if (result[i]>9){
                    result[i-1] = result[i-1] + result[i]/10 ;
                    result[i] = result[i] % 10;
                }

            }
        }
       int k = 0;
       if(result[result.length-1] == 0 ){
           k = 1;
       }
       long results[] = new long[result.length-k];
       for (int i = 0; i <results.length ; i++) {
           results[i] = result[i];

       }

        return results;
    }
    public static boolean allLessTen(long[] arr){
        for (int i = 0; i <arr.length ; i++) {
            if (arr[i]>9) return false;

        }return true;
    }

}
