/**
* @author Victoria Gorskaya
* 11-801
* Task 26 
*/
package ru.itis.kpfu;

public class Task26 {
//26
//Проверить, что в трехмерном массиве в каждом его двумерном массиве существует такая строка, что в ней все элементы делятся на три.
    public static boolean  task26(int [][][] array){
        //boolean flag = true;
        int i = 0;
        while((i < array.length)){
            boolean flag1 = false;
            int j = 0;
            while (!flag1 && j < array[i].length){
                int k = 0;
                while (k < array[i][j].length && ((array[i][j][k] % 3) == 0)){
                    k++;
                }
                if(k == array[i][j].length){
                    flag1 = true;
                }
                j++;
                k = 0;
            }
            if(!flag1){
                return false;
            }
            flag1 = false;
            i++;


        }return true;

    }

}
