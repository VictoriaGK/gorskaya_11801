package ru.itis.kpfu;

public class Vector2D {
    private double x;
    private double y;

    public Vector2D(double x, double y){
        this.x = x;
        this.y = y;

    }
    public Vector2D(){
        this.x = 0;
        this.y = 0;
    }

    public void set(double x, double y){
        this.x = x;
        this.y = y;
    }

    public  double getX(){
        return x;
    }

    public  double getY(){
        return y;
    }


    public Vector2D add(Vector2D v1) {
        return new Vector2D(x + v1.getX(), y + v1.getY());

    }

    public static   Vector2D sum(Vector2D v1, Vector2D v2){
        return new Vector2D(v1.getX()+ v2.getX(), v1.getY()+ v2.getY());

    }

    public Vector2D sub(Vector2D v1){
        return new Vector2D(x- v1.getX(), y- v1.getY());
    }


    public void add2(Vector2D v1){
        x += v1.getX();
        y += v1.getY();
    }
    public void sub2(Vector2D v1){
        x += v1.getX();
        y += v1.getY();
    }


    public  static   Vector2D nosum(Vector2D v1, Vector2D v2){
        return new Vector2D(v1.getX()- v2.getX(), v1.getY()- v2.getY());

    }

    public Vector2D mult(double c){
        return new Vector2D(c*x, c*y);
    }

    public String toString(){
        return "( " + x+ "; "+ y +" )";
    }
    public void mult2(double c){
        x = c*x;
        y = c*y;
    }
    public double length(){
        return Math.sqrt(x*x+y*y);
    }
    public double scalarProduct(Vector2D v1) {
        return v1.getX()*x + v1.getY()*y;
    }

    public double cos(Vector2D v1){
        double sqHelp1 = Math.sqrt(x*x+y*y);
        double sqHelp = Math.sqrt((v1.getX()*v1.getX())+(v1.getY()*v1.getY()));
        return (v1.getX()*x + v1.getY()*y)/(sqHelp*sqHelp1);
    }

    public boolean equals(Vector2D v1){
        if((v1.getX()==x)&&(v1.getY()==y)){
            return true;
        } else return false;
    }
}
