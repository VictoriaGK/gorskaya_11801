/**
* @author Victoria Gorskaya
* 11-801
* Task 14 
*/
package ru.itis.kpfu;

import java.util.Scanner;

public class Task14 {
    public static void task14(){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] array = new int[n];
        int k = 1;
        for (int i = 0; i < n; i++) {
            array[i] = k;
            if (k > 0) k = -(k + 2);
            else k = -k + 2;

            System.out.println(array[i]);
        }
    }
}
