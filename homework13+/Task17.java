/**
* @author Victoria Gorskaya
* 11-801
* Task 17 
*/
package ru.itis.kpfu;

import java.util.Scanner;

public class Task17 {
    public static int[] task17(int[] ar) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int m = k;
        for (int i = 0; i < ar.length - k; i++) {
            int array = ar[ar.length-1];
            for(int j = ar.length-1; j>=1; j--){
                ar[j] = ar[j-1];
            }
            ar[0] = array;
        }
        return ar;

    }
}