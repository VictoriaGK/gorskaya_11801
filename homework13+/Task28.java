/**
* @author Victoria Gorskaya
* 11-801
* Task 28 
*/
package ru.itis.kpfu;

public class Task28 {
    public static String[] task28(String[] s){
        for (int i = 0; i <s.length; i++) {
            String string = null;
            boolean flag  = true;
            int j = i+1;
            while ( j< s.length) {
                if (Task27.task27(s[i], s[j]) == 1) {
                    string = s[i];
                    s[i] = s[j];
                    s[j] = string;
                }
                j++;
            }
        }return s;

    }

}
