/**
* @author Victoria Gorskaya
* 11-801
* Task 37 
*/
package ru.itis.kpfu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task37 {

    public static void task37Matches(){
        Pattern p = Pattern.compile("[02468]{4,6}");
        boolean flag = true;
        int k = 0;
        int count = 0;
        while(flag){
            int a = (int)(Math.random()*1000000);
            System.out.println(a);
            String s = String.valueOf(a);
            Matcher m = p.matcher(s);
            count++;
            if (m.matches()){
                k++;
                System.out.println(a);
            }
            if(k==10){
                flag = false;
            }

        }
        System.out.println(count);


    }



    public static void task37Find(){
        Pattern p = Pattern.compile("[02468]{4,6}");
        boolean flag = true;
        int k = 0;
        int count = 0;
        while(flag){
            int a = (int)(Math.random()*1000000);

            String s = String.valueOf(a);
            Matcher m = p.matcher(s);
            count++;
            if (m.find()){
                k++;
                System.out.println(a);
            }
            if(k==10){
                flag = false;
            }

        }
        System.out.println(count);
    }
}
