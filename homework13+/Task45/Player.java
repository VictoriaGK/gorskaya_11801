package ru.itis.kpfu;

public class Player {
    int hp = 10;
    String name;

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void hit(Player player, int k){
        double chance = (((9-k)*100)/8);
        System.out.println(chance);
        double random = Math.random()*100;
        System.out.println(random);
        if(random <= chance) {
            player.setHp(player.getHp() - k);
        }
    }
}

