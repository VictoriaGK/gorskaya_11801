/**
* @author Victoria Gorskaya
* 11-801
* Task 35 
*/
package ru.itis.kpfu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task35new {
    public static boolean task35(String s){
        Pattern p = Pattern.compile("(0+)|(1+)|((01)*0?)|((10)*1?)");
        Matcher m = p.matcher(s);
        return m.matches();

    }
}
