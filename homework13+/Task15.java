/**
* @author Victoria Gorskaya
* 11-801
* Task 15 
*/
package ru.itis.kpfu;

public class Task15 {
    public static void task15(String[] args){
        int k = 0;
        for(int i = 0; i < args.length; i++) {
            k = 10*k + Integer.parseInt(args[i]);
            System.out.println("Argument is: "+args[i]);
        }
        System.out.println(k);
    }

    public static void task21(String[] args){
        int k = 0;
        int d = 1;
        for(int i = 0; i < args.length; i++) {
            k = d*Integer.parseInt(args[i]) +k ;
            System.out.println("Argument is: "+args[i]);
            d = d*10;
        }
        System.out.println(k);

    }
}
