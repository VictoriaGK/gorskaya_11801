/**
* @author Victoria Gorskaya
* 11-801
* Task 20 
*/
package ru.itis.kpfu;

public class Task20 {
    public static int task20(int[][] a) {
        int k = 0;
        int dif = 0;
        int[] max = new int[a.length];
        for (int i = 0; i < a.length; i++) {

            for (int j = 0; j < a[i].length; j++) {
                max[i] = max[i] + a[j][(j + dif) % a[i].length];

            }
            dif++;
        }
        for (int i = 0; i < a.length; i++) {
            if (max[i] > k)
                k = max[i];

        }
        return k;
    }

}
