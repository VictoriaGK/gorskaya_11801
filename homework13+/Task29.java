/**
* @author Victoria Gorskaya
* 11-801
* Task 29 
*/
package ru.itis.kpfu;

public class Task29 {
    public static int task29(String s){
        int k = 0;
        String[] strings = s.split("\\s");
        for (int i = 0; i <strings.length ; i++) {
            if (strings[i].charAt(0)< "a".charAt(0)){
                k++;

            }

        }return k;
    }
}
