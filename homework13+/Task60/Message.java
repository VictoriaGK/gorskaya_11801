package ru.itis.kpfu;

public class Message {
    private  User sender_id;
    private User receiver_id;
    private String timesent;
    private String text;
    private String status;

    public Message(User sender_id, User receiver_id, String timesent, String text, String status) {
        this.sender_id = sender_id;
        this.receiver_id = receiver_id;
        this.timesent = timesent;
        this.text = text;
        this.status = status;
    }
    public Message(){}

    public User getSender_id() {
        return sender_id;
    }

    public User getReceiver_id() {
        return receiver_id;
    }

    public String getTimesent() {
        return timesent;
    }

    public String getText() {
        return text;
    }

    public String getStatus() {
        return status;
    }

    public void setSender_id(User sender_id) {
        this.sender_id = sender_id;
    }

    public void setReceiver_id(User receiver_id) {
        this.receiver_id = receiver_id;
    }

    @Override
    public String toString() {
        return "Message{" +
                "sender_id=" + sender_id +
                ", receiver_id=" + receiver_id +
                ", timesent='" + timesent + '\'' +
                ", text='" + text + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
