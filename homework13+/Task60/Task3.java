package ru.itis.kpfu;

import java.util.ArrayList;

public class Task3 {
    public static String task3(ArrayList<Message> messages){
        if(numMale(messages) == numFemale(messages)) return "nobody";
        if(numFemale(messages)> numMale(messages)){
            return "Female";
        }
        return "Male";
    }
    public static int numFemale(ArrayList<Message> messages){
        int k = 0;
        for(Message message: messages){
            if(message.getSender_id().getGender().equals("female")){
                k++;
            }
        }return k;
    }
    public static int numMale(ArrayList<Message> messages){
       return messages.size() - numFemale(messages);
    }
}
