package ru.itis.kpfu;

import java.util.ArrayList;

public class Task1 {
    public static void task1(User user1, User user2, ArrayList<Message> messages){
        for(Message message: messages){
            if(fromTo(user1, user2, message)){
                System.out.println(message.getText());
            }

        }

    }

    public static boolean fromTo(User user1, User user2, Message message){
        if(((message.getReceiver_id().equals(user1)) && (message.getSender_id().equals(user2))) ||
                ((message.getReceiver_id().equals(user2)) && (message.getSender_id().equals(user1)))){
            return true;
        }return false;

    }
}
