package ru.itis.kpfu;

import java.util.ArrayList;

public class Task4 {
    public static void task4(ArrayList<Message> messages){
        int allMessageFromFemale = 0;
        int allMessageFromMale = 0;
        int mesCheckFem = 0;
        int mesCheckMale = 0;
        for (Message message: messages){
            if(fromGender(message, "female", "male")){
                allMessageFromFemale++;
                if(checkOrNot(message)){
                    mesCheckFem++;
                }
            }if(fromGender(message, "male", "female")){
                allMessageFromMale++;
                if(checkOrNot(message)){
                    mesCheckMale++;
                }
            }

        }
        System.out.println((mesCheckFem*100/allMessageFromFemale));
        System.out.println((mesCheckMale*100/allMessageFromMale));

    }

    public static boolean fromGender(Message message, String who, String whom){
        if ((message.getSender_id().getGender().equals(who))&& message.getReceiver_id().getGender().equals(whom)) {
            return true;
        }
         return false;

    }
    public static boolean checkOrNot(Message message){
        if(message.getStatus().equals("1")) return false;
        return true;
    }
}
