package ru.itis.kpfu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("users.txt"));
        ArrayList<User> users = new ArrayList<User>();
        while (sc.hasNextLine()) {
            String[] s = sc.nextLine().split("\\s+");
            User user = new User(Integer.parseInt(s[0]), s[1], s[2], s[3], s[4]);
            users.add(user);
        }
        sc = new Scanner(new File("messages.txt"));
        ArrayList<Message> messages = new ArrayList<>();
        while (sc.hasNextLine()){
            String[] s = sc.nextLine().split("\\s+");
            Message message = new Message(null, null, s[2], s[3], s[4]);
            for (User user: users) {
                if (user.getId() == Integer.parseInt(s[0])) {
                    message.setSender_id(user);
                    continue;

                }
                if (user.getId() == Integer.parseInt(s[1])) {
                    message.setReceiver_id(user);

                }
            }messages.add(message);
        }

        sc = new Scanner(new File("subs.txt"));
        ArrayList<Sub> subs = new ArrayList<>();
        while (sc.hasNextLine()){
            String[] s = sc.nextLine().split("\\s+");
            Sub sub = new Sub();
            for (User user: users) {
                if (user.getId() == Integer.parseInt(s[0])) {
                    sub.setSubscriber_id(user);
                    continue;
                }
                if (user.getId() == Integer.parseInt(s[1])) {
                    sub.setSubscription_id(user);
                }
            }subs.add(sub);
        }
       // Task1.task1(users.get(0), users.get(1), messages);
        //Task2.task2(users.get(0), subs);
        System.out.println(Task3.task3(messages));
        Task4.task4(messages);
    }
}
