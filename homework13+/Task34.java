/**
* @author Victoria Gorskaya
* 11-801
* Task 34 
*/
package ru.itis.kpfu;

public class Task34 {
    ^([-+]{0,1}(([1-9]\d*[.,]?\d*[1-9])|(\d+[.,]?\d*\(\d+\))|([1-9]+[.,]?)|([0][,.]\d*[1-9])))|[0]$
}
