/**
* @author Victoria Gorskaya
* 11-801
* Task 30 
*/
package ru.itis.kpfu;

public class Task30 {
    public static void task30(String s){
        for (int i = (int)('a'); i < (int)('z'); i++){
            System.out.print((char)i);
            System.out.println(find((char)i,s));

        }
    }


    public static int find(char ch, String s){
        int k = 0;
        char charSt;
        for (int i = 0; i <s.length() ; i++) {
            if(s.charAt(i) <= 'Z') {
                charSt = (char)((int)(s.charAt(i)) + 32);
            }else {
                charSt = s.charAt(i);
            }
            if(ch == charSt)
                k++;
        }return k;

    }
}
