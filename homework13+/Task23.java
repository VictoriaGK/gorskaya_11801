/**
* @author Victoria Gorskaya
* 11-801
* Task 23 
*/
package ru.itis.kpfu;

public class Task23 {
    public static double[][] task23(double[][] arr){
        int trian = 0;
        for (int j = 0; j < arr.length; j++) {
            double k = arr[0+trian][j];
            for (int i = 1+trian; i <arr.length ; i++) {
                double n = - (arr[i][j]/k);
                for (int l = 0; l < arr.length; l++) {
                    arr[i][l] = arr[i][l] + arr[0+trian][l]*n;
                }
            }
            trian++;

        } return arr;
    }
}
