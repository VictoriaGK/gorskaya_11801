/**
* @author Victoria Gorskaya
* 11-801
* Task 22 
*/
package ru.itis.kpfu;

import java.util.Scanner;

public class Task22 {
    public static int [][] task22(int [][] arr){
        int n = (arr.length - 1)/2;
        int k = n;
        for (int i = 0; i <arr.length ; i++) {
            for (int j = 1+ n-Math.abs(k); j < n+Math.abs(k); j++) {
                arr[i][j] = 0;
            }
            k--;
        }return arr;

    }
}
