package ru.itis.kpfu;

public class RationalFraction {
    private int x;
    private int y;
    public RationalFraction() {
        this.x = 0;
        this.y = 0;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public  RationalFraction(int x, int y){
        this.x = x;
        this.y = y;
    }
    public  void reduce(){
        int x1 = x;
        int y1 = y;
        if(x == 0){
            x1 = 1; y1 = 1;
        }
        while((x1!=1 || y1!=1) ) {
            x1 = x;
            y1 = y;

            while (x1 != y1) {
                if (x1 > y1)
                    x1 = x1 - y1;
                else
                    y1 = y1 - x1;
            }
            x = x / x1;
            y = y / y1;
        }
    }


    public RationalFraction add(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*y2;
        int summ = x*y2 + x2*y;
        RationalFraction fractionAdd =  new RationalFraction(summ,denom);
        fractionAdd.reduce();
        return  fractionAdd;

    }

    public void add2(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*y2;
        int summ = x*y2 + x2*y;
        x = summ;
        y = denom;
        this.reduce();

    }

    public  RationalFraction sub(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*y2;
        int subb = x*y2 - x2*y;
        RationalFraction fraction1 = new RationalFraction(subb, denom);
        fraction1.reduce();
        return fraction1;


    }

    public void sub2(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*y2;
        int subb = x*y2 - x2*y;
        x = subb;
        y = denom;
        this.reduce();


    };

    public RationalFraction mult(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*y2;
        int subb = x*x2;
        RationalFraction fraction1 = new RationalFraction(subb, denom);
        fraction1.reduce();
        return fraction1;

    }

    public void mult2(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*y2;
        int subb = x*x2;
        x = subb;
        y = denom;
        this.reduce();
    }


    public RationalFraction div(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*x2;
        int subb = x*y2;
        RationalFraction fraction1 = new RationalFraction(subb, denom);
        fraction1.reduce();
        return fraction1;

    }

    public void div2(RationalFraction fraction){
        int y2 = fraction.getY();
        int x2 = fraction.getX();
        int denom = y*x2;
        int subb = x*y2;
        x = subb;
        y = denom;
        this.reduce();


    }


    public String toString() {
        return
                x +
                "/" + y;
    }

    public double value(){
        double fraction = x/y;
        return fraction;
    };


    public boolean equals(RationalFraction fraction){
        this.reduce();
        fraction.reduce();
        if((x == fraction.getX()) && (y == fraction.getY())){
            return true;
        }
        return false;
    }

    public int numberPart(){
        int numb = x/y;
        return numb;
    }

}
