package ru.itis.kpfu;

public class RationalVector2D {
    private RationalFraction xratFr;
    private RationalFraction yratFr;

    public RationalFraction getXratFr() {
        return xratFr;
    }

    public void setXratFr(RationalFraction xratFr) {
        this.xratFr = xratFr;
    }

    public RationalFraction getYratFr() {
        return yratFr;
    }

    public void setYratFr(RationalFraction yratFr) {
        this.yratFr = yratFr;
    }

    public RationalVector2D(RationalFraction xratFr, RationalFraction yratFr) {
        this.xratFr = xratFr;
        this.yratFr = yratFr;
    }
    public RationalVector2D() {
        xratFr = null;
        yratFr = null;
    }
    public RationalVector2D add(RationalVector2D rationalVector2D){
        RationalVector2D vector2D = new RationalVector2D(xratFr.add(rationalVector2D.getXratFr()),yratFr.add(rationalVector2D.getYratFr()));
        return vector2D;

    }


    public String toString() {
        return "(" + xratFr.toString() + ","+ yratFr.toString() + ")";
    }



	public double length(){
        RationalFraction rationalFraction = xratFr.mult(xratFr).add(yratFr.mult(yratFr));

        return  Math.sqrt(rationalFraction.getX())/ Math.sqrt(rationalFraction.getY());
    }


	public RationalFraction scalarProduct(RationalVector2D rationalVector2D){

        return  new RationalFraction(((xratFr.mult(rationalVector2D.getXratFr())).add(yratFr.mult(rationalVector2D.getYratFr()))).getX(),
        (xratFr.mult(rationalVector2D.getXratFr())).add(yratFr.mult(rationalVector2D.getYratFr())).getY());

    }
	public boolean equals(RationalVector2D rationalVector2D){
        if ((xratFr.equals(rationalVector2D.getXratFr())) && (yratFr.equals(rationalVector2D.getYratFr()))){
            return true;
        }
        return false;

    }
}
