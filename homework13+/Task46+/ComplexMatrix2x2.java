package ru.itis.kpfu;

public class ComplexMatrix2x2 {
    private ComplexNumber[][] matrix;

    public ComplexMatrix2x2(){
        matrix = new ComplexNumber[2][2];
        matrix[0][0] = null;
        matrix[0][1] = null;
        matrix[1][0] = null;
        matrix[1][1] = null;
    }

    public ComplexNumber[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(ComplexNumber[][] matrix) {
        this.matrix = matrix;
    }

    public ComplexMatrix2x2(ComplexNumber[][] matrix) {
        this.matrix = matrix;
    }
    public ComplexMatrix2x2(ComplexNumber aa, ComplexNumber ab, ComplexNumber ba, ComplexNumber bb){
        matrix = new ComplexNumber[2][2];

        matrix[0][0] = aa;
        matrix[0][1] = ab;
        matrix[1][0] = ba;
        matrix[1][1] = bb;
    }
    public ComplexMatrix2x2(ComplexNumber aa){
        matrix = new ComplexNumber[2][2];

        matrix[0][0] = aa;
        matrix[0][1] = aa;
        matrix[1][0] = aa;
        matrix[1][1] = aa;
    }
    ComplexMatrix2x2 add(ComplexMatrix2x2) - сложение матрицы с другой;
•	ComplexMatrix2x2 mult(ComplexMatrix2x2) - умножение матрицы на другую матрицу;
•	ComplexNumber det() - определитель матрицы;
•	ComplexVector2D multVector(ComplexVector2D) - умножить матрицу на двумерный комплекснозначный вектор (считая его столбцом) и возвратить получившийся столбец в виде вектора.




}
