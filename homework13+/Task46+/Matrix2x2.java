package ru.itis.kpfu;

import java.util.Arrays;

public class Matrix2x2 {

    private double[][] matrix;

    public Matrix2x2(){
        matrix = new double[2][2];
        matrix[0][0] = 0;
        matrix[0][1] = 0;
        matrix[1][0] = 0;
        matrix[1][1] = 0;
    }

    public Matrix2x2(double[][] matrix){
        this.matrix = matrix;
    }

    public Matrix2x2(double aa, double ab, double ba, double bb){
        matrix = new double[2][2];


        matrix[0][0] = aa;
        matrix[0][1] = ab;
        matrix[1][0] = ba;
        matrix[1][1] = bb;
    }


    @Override
    public String toString() {
        return matrix[0][0] + " "+
                matrix[0][1] + "\n" +
                matrix[1][0] + " " +
                matrix[1][1];
    }

    public Matrix2x2 (double a) {
        matrix = new double[2][2];

        new Matrix2x2(a, a, a, a);
    }

    public Matrix2x2 add(Matrix2x2 matrix){
        return new Matrix2x2(
                this.matrix[0][0] + matrix.getMatrix()[0][0],
                this.matrix[0][1] + matrix.getMatrix()[0][1],
                this.matrix[1][0] + matrix.getMatrix()[1][0],
                this.matrix[1][1] + matrix.getMatrix()[1][1]
        );
    }

    public void add2(Matrix2x2 matrix){
        this.matrix = add(matrix).getMatrix();
    }

    public  Matrix2x2 sub(Matrix2x2 matrix){
        return new Matrix2x2(
                this.matrix[0][0] - matrix.getMatrix()[0][0],
                this.matrix[0][1] - matrix.getMatrix()[0][1],
                this.matrix[1][0] - matrix.getMatrix()[1][0],
                this.matrix[1][1] - matrix.getMatrix()[1][1]
        );
    }

    public void sub2(Matrix2x2 matrix){
        this.matrix = sub(matrix).getMatrix();
    }

    public Matrix2x2 multiMatrix(Matrix2x2 matrix){
        return new Matrix2x2(
                this.matrix[0][0]*matrix.getMatrix()[0][0]+this.matrix[0][1]*matrix.getMatrix()[0][1],
                this.matrix[0][0]*matrix.getMatrix()[1][0]+this.matrix[0][1]*matrix.getMatrix()[1][1],
                this.matrix[1][1]*matrix.getMatrix()[0][0]+this.matrix[1][1]*matrix.getMatrix()[0][1],
                this.matrix[1][1]*matrix.getMatrix()[1][0]+this.matrix[1][1]*matrix.getMatrix()[1][1]
        );
    }

    public void multiMatrix2(Matrix2x2 matrix){
        this.matrix = multiMatrix(matrix).getMatrix();
    }

    public Matrix2x2 multNumber(double a){
        return  new Matrix2x2(matrix[0][0]*a , matrix[1][0]*a,
                matrix[0][1]*a,  matrix[1][1]*a);
    }

    public void multiNumber2(double a){
        this.matrix = multNumber(a).getMatrix();
    }

    public double det(){
        return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];
    }

    public void transpon(){
        double a = matrix[1][0];
        matrix[1][0] = matrix[0][1];
        matrix[0][1] = a;
    }

    public Matrix2x2 inverse() throws Exception {
        if(det()==0){
            throw new Exception("Cant get inverse matrix");
        }else {
            Matrix2x2 matrix2x2 = new Matrix2x2(
                    matrix[1][1], -matrix[0][1],
                    -matrix[1][0], matrix[0][0]);
            matrix2x2.transpon();
            return matrix2x2.multNumber(-1/2);
        }
    }

    public Matrix2x2 equivalentDiagonal(){
        return new Matrix2x2(
                matrix[0][0], 0,
                0,matrix[1][1]);
    }

//    public Vector2D multVector(Vector2D v2){
//    }


    private void createMatrix(){
        this.matrix = new double[2][2];
    }

    public double[][] getMatrix() {
        return matrix;
    }

    public static void main(String[] args) {
        double[][] mat = new double[3][3];
        mat[1][2] = 3;
        mat[1][1] = 5;
        Matrix2x2 matrix2x2 = new Matrix2x2(mat);
    }
}
