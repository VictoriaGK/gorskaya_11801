package ru.itis.kpfu;

public class ComplexVector2D {
    private ComplexNumber xCN;
    private ComplexNumber yCN;

    public ComplexVector2D(){
        xCN = null;
        yCN = null;
    }

    public ComplexVector2D(ComplexNumber xCN, ComplexNumber yCN) {
        this.xCN = xCN;
        this.yCN = yCN;
    }

    public ComplexNumber getxCN() {
        return xCN;
    }

    public void setxCN(ComplexNumber xCN) {
        this.xCN = xCN;
    }

    public ComplexNumber getyCN() {
        return yCN;
    }

    public void setyCN(ComplexNumber yCN) {
        this.yCN = yCN;
    }

    public ComplexVector2D add(ComplexVector2D complexVector2D){
        ComplexVector2D complexVector2D1 = new ComplexVector2D(xCN.add(complexVector2D.getxCN()),yCN.add(complexVector2D.getyCN()));
        return complexVector2D1;

    }
	public String toString(){
       return  "("+ xCN.toString() + ", " + yCN.toString() + " )";
    }

	public ComplexNumber scalarProduct(ComplexVector2D complexVector2D){
        ComplexNumber c1 = xCN.mult(complexVector2D.getxCN());
        ComplexNumber c2 = yCN.mult(complexVector2D.getyCN());
        c1 = c1.add(c2);
        return  c1;
    }

	boolean equals(ComplexVector2D complexVector2D) {
       if((xCN == complexVector2D.xCN) && (yCN == complexVector2D.yCN)){
         return true;
       }return false;
    }


}
