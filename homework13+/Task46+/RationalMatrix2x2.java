package ru.itis.kpfu;

import java.util.Arrays;

public class RationalMatrix2x2 {
    private RationalFraction[][] matrix;


    public RationalMatrix2x2(){
        matrix = new RationalFraction[2][2];
        matrix[0][0] = null;
        matrix[0][1] = null;
        matrix[1][0] = null;
        matrix[1][1] = null;
    }

    public RationalFraction[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(RationalFraction[][] matrix) {

        this.matrix = matrix;
    }

    public RationalMatrix2x2(RationalFraction aa, RationalFraction ab, RationalFraction ba, RationalFraction bb){
        matrix = new RationalFraction[2][2];

        matrix[0][0] = aa;
        matrix[0][1] = ab;
        matrix[1][0] = ba;
        matrix[1][1] = bb;
    }
    public RationalMatrix2x2(RationalFraction aa){
        matrix = new RationalFraction[2][2];


        matrix[0][0] = aa;
        matrix[0][1] = aa;
        matrix[1][0] = aa;
        matrix[1][1] = aa;
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 m){
        return new RationalMatrix2x2(
               matrix[0][0].add(m.getMatrix()[0][0]),
                matrix[0][1].add(m.getMatrix()[0][1]),
                matrix[1][0].add(m.getMatrix()[1][0]),
                matrix[1][1].add(m.getMatrix()[1][1])
        );

    }


    RationalMatrix2x2 mult(RationalMatrix2x2 matrix){
        return new RationalMatrix2x2(
                this.matrix[0][0].mult(matrix.getMatrix()[0][0]).add(this.matrix[0][1].mult(matrix.getMatrix()[0][1])),
                this.matrix[0][0].mult(matrix.getMatrix()[1][0]).add(this.matrix[0][1].mult(matrix.getMatrix()[1][1])),
                this.matrix[1][1].mult(matrix.getMatrix()[0][0]).add(this.matrix[1][1].mult(matrix.getMatrix()[0][1])),
                this.matrix[1][1].mult(matrix.getMatrix()[1][0]).add(this.matrix[1][1].mult(matrix.getMatrix()[1][1]))
        );

    }

    @Override
    public String toString() {
        return matrix[0][0].toString() + " "+
        matrix[0][1].toString() + "\n" +
        matrix[1][0] .toString() + " " +
        matrix[1][1].toString() ;
    }

    public RationalFraction det(){

        return ((matrix[0][0].mult(matrix[1][1])).sub(matrix[0][1].mult(matrix[1][0])));

    }

	public RationalVector2D multVector(RationalVector2D v1){
        return new RationalVector2D(v1.getXratFr().mult(matrix[0][0]).add(v1.getYratFr().mult(matrix[0][1])),
        v1.getXratFr().mult(matrix[1][0]).add(v1.getYratFr().mult(matrix[1][1])));

    }





}
