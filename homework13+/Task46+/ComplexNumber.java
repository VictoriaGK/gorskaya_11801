package ru.itis.kpfu;

public class ComplexNumber {
    private double i;
    private double j;


    public ComplexNumber(){
        this.i = 0;
        this.j = 0;

    }
    public ComplexNumber(double i, double j){
        this.i = i;
        this.j = j;

    }
    public void set(double i, double j){
        this.i=i;
        this.j=j;
    }

    public  double getI(){
        return i;
    }

    public  double getJ(){
        return j;
    }

    public ComplexNumber add(ComplexNumber cmpl){
        return new ComplexNumber(i+ cmpl.getI(), j+ cmpl.getJ());


    }

    public  void add2(ComplexNumber cmpl){
        i += cmpl.getI();
        j += cmpl.getJ();


    }

    public ComplexNumber sub(ComplexNumber cmpl) {
        return new ComplexNumber(i- cmpl.getI(), j- cmpl.getJ());

    }
    public  void sub2(ComplexNumber cmpl){
        i = i- cmpl.getI();
        j = j- cmpl.getJ();


    }

    public ComplexNumber multNumber(double c){
        return new ComplexNumber(c*i, c*j);
    }

    public void multNumber2(double c){
        i = i*c;
        j = j*c;
    }

    public ComplexNumber div(ComplexNumber complexNumber){
        double c = -(complexNumber.getI())*complexNumber.getI() + (-complexNumber.getJ())*complexNumber.getJ();
        double i2 = ((j* complexNumber.getI() + i*(-complexNumber.getJ()))/c);
        double  j2 = ( -i*complexNumber.getI()+ j*complexNumber.getJ())/c;
        return new ComplexNumber(i2,j2);

    }

    @Override
    public String toString() {
        return i +"i" +
                "  + " + j ;
    }
    public 	ComplexNumber mult(ComplexNumber complexNumber){
        double i2 = ((j* complexNumber.getI() + i*(-complexNumber.getJ())));
        double  j2 = ( -i*complexNumber.getI()+ j*complexNumber.getJ());
        return new ComplexNumber(i2,j2);

    }
	public void mult2(ComplexNumber complexNumber){
        double i2 = ((j* complexNumber.getI() + i*(-complexNumber.getJ())));
        double  j2 = ( -i*complexNumber.getI()+ j*complexNumber.getJ());
        i = i2;
        j = j2;

    };


}
