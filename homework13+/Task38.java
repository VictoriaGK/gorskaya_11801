/**
* @author Victoria Gorskaya
* 11-801
* Task 38 
*/
package ru.itis.kpfu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task38 {
    public static void task38Matches(){
        Pattern p = Pattern.compile("[13579]*([02468][02468][13579]*){2}[13579]*");
        boolean flag = true;
        int k = 0;
        int count = 0;
        while(flag){
            int a = (int)(Math.random()*100000);
            System.out.println(a);
            String s = String.valueOf(a);
            Matcher m = p.matcher(s);
            count++;
            if (m.matches()){
                k++;
                System.out.println(a);
            }
            if(k==10){
                flag = false;
            }

        }
        System.out.println(count);


    }



    public static void task38Find(){
        Pattern p = Pattern.compile("[13579]*([02468][02468][13579]*){2}[13579]*");
        boolean flag = true;
        int k = 0;
        int count = 0;
        while(flag){
            int a = (int)(Math.random()*100000);

            String s = String.valueOf(a);
            Matcher m = p.matcher(s);
            count++;
            if (m.find()){
                k++;
                System.out.println(a);
            }
            if(k==10){
                flag = false;
            }

        }
        System.out.println(count);
    }
}
