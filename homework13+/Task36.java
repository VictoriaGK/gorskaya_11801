/**
* @author Victoria Gorskaya
* 11-801
* Task 36 
*/
package ru.itis.kpfu;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task36 {
    //Генерировать случайные положительные целые числа c помощью java.util.Random.
    // Вывести первые 10 сгенерированных чисел, в которых нет трех четных цифр подряд.
    // Остановить генератор, вывести общее количество сгенерированных чисел.
    // Проверку осуществлять регулярным выражением.
    // НЕ использовать математические операции для анализа числа (сделать двумя способами – с помощью matches и с помощью find)
    public static void task36Matches(){
        Pattern p = Pattern.compile("[^02486]*(([02486]{0,2}[^02468]+)*)[02468]{0,2}");
        boolean flag = true;
        int k = 0;
        int count = 0;
        while(flag){
            int a = (int)(Math.random()*1000000000);
            System.out.println(a);
            String s = String.valueOf(a);
            Matcher m = p.matcher(s);
            count++;
            if (m.matches()){
                k++;
                System.out.println(a);
            }
            if(k==10){
                flag = false;
            }

        }
        System.out.println(count);
//        return m.matches();

    }
    public static void task36Find(){
        Pattern p = Pattern.compile("[^02486]*(([02486]{0,2}[^02468]+)*)[02468]{0,2}");
        boolean flag = true;
        int k = 0;
        int count = 0;
        while(flag){
            int a = (int)(Math.random()*1000000000);
            System.out.println(a);
            String s = String.valueOf(a);
            Matcher m = p.matcher(s);
            count++;
            if (m.find()){
                k++;
                System.out.println(a);
            }
            if(k==10){
                flag = false;
            }

        }
        System.out.println(count);
    }

}
