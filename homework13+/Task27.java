/**
* @author Victoria Gorskaya
* 11-801
* Task 27 
*/
package ru.itis.kpfu;

public class Task27 {

            public static int task27(String s1, String s2){
                int k = 0;
                if(s1.length()>=s2.length()){
                    k = s1.length();
                }else k = s2.length();
                for (int i = 0; i <k ; i++) {
                    if(s1.charAt(i)!=s2.charAt(i)){
                        if(s1.charAt(i)>s2.charAt(i)){
                            return 1;
                        }else return -1;
                    }
                }return 0;
            }

}
