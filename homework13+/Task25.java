/**
* @author Victoria Gorskaya
* 11-801
* Task 25 
*/
package ru.itis.kpfu;

public class Task25 {
    public static void task25(int [][] mA, int [][] mB){
        int m = mA.length;
        int n = mB[0].length;
        int o = mB.length;
        int[][] res = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < o; k++) {
                    res[i][j] += mA[i][k] * mB[k][j];
                }
            }
        }

        for (int i = 0; i < res.length; i++) {
            for (int j = 0; j < res[0].length; j++) {
                System.out.print(res[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }

    }
}
