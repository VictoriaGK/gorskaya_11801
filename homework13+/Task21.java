/**
* @author Victoria Gorskaya
* 11-801
* Task 21 
*/
package ru.itis.kpfu;

public class Task21 {
    public static boolean task21(int[][] a) {
        int dif = 0;
        int[] max = new int[a.length];
        for (int i = 0; i <a.length; i++) {
            for (int j = a.length - 1; j >= 0; j--) {
                max[i] = max[i] + a[j][(dif + a.length - 1 - j) % a[i].length];
            }
            dif++;
            if (max[i] % 2 != 0)
                return false;
        }
        return true;
    }

}
