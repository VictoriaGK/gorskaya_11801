package ru.itis.kpfu;

public class Elemm {
    int kf;
    int st;
    Elemm next;

    public Elemm(int kf, int st) {
        this.kf = kf;
        this.st = st;

    }

    public int getKf() {
        return kf;
    }

    public int getSt() {
        return st;
    }

    public void setKf(int kf) {
        this.kf = kf;
    }

    public void setSt(int st) {
        this.st = st;
    }

    public void setNext(Elemm next) {
        this.next = next;
    }

    public Elemm getNext() {
        return next;
    }
}
