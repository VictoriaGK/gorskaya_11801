package ru.itis.kpfu;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Polinom {
    Elemm head;

    public Polinom(Elemm head) {
        this.head = head;
    }

    public Elemm getHead() {
        return head;
    }

    public Polinom(String filename) throws FileNotFoundException {

        Scanner sc = new Scanner(new File(filename));

        while (sc.hasNextLine()) {
            String[] s = sc.nextLine().split(" ");
            insert(Integer.parseInt(s[0]), Integer.parseInt(s[1]));

        }

    }


    public void insert(int coef, int deg){

        if (head == null){
            Elemm element = new Elemm(coef, deg);
            head = element;
            return;
        }
        Elemm el2 = head;
        if(deg > el2.getSt()){
            Elemm element = new Elemm(coef, deg);
            element.setNext(head);
            head = element;
            return;
        } else{
            while(el2.getNext()!=null){
                if(deg == el2.getSt()){
                    Elemm element = new Elemm(coef, deg);
                    element.setNext(el2.getNext());
                    el2.setNext(element);
                    return;
                }else{
                    if(deg<el2.getSt()&& deg>el2.getNext().getSt()){
                        Elemm element = new Elemm(coef, deg);
                        element.setNext(el2.getNext());
                        el2.setNext(element);
                        return;
                    }
                }

                el2 = el2.getNext();
            }
            if(deg == el2.getSt()){
                Elemm element = new Elemm(coef, deg);
                element.setNext(el2.getNext());
                el2.setNext(element);
                return;
            }
            if(deg<el2.getSt()){
                Elemm element = new Elemm(coef, deg);
                element.setNext(el2.getNext());
                el2.setNext(element);

            }

        }

    }

    public void combine() {
        Elemm el = head;

        while (el.getNext() != null) {
            if (el.getNext().getSt()==el.getSt()){
                el.setKf(el.getKf()+ el.getNext().getKf());
                if(el.getNext().getNext()!=null){
                    el.setNext(el.getNext().getNext());
                }else
                {
                    el.setNext(null);
                }
            }
            el = el.getNext();

        }
    }
    public void delete(int deg) {
        Elemm el = head;

        while (el.getNext() != null) {
            if (el.getNext().getSt()==deg){
                if(el.getNext().getNext()!=null){
                    el.setNext(el.getNext().getNext());
                    return;
                }else
                {
                    el.setNext(null);
                    return;
                }
            }
            el = el.getNext();

        }
    }

    public void sum(Polinom p){
        Elemm el = head;
        if(el.getSt() == p.head.getSt()){
            el.setKf( el.getKf()+p.head.getSt());
            p.head = p.head.getNext();
        }
        while (el.getNext() != null) {
            if (el.getNext().getSt()==p.head.getSt()){
                el.getNext().setKf( el.getNext().getKf()+p.head.getSt());
                p.head = p.head.getNext();
            }
            el = el.getNext();

        }

    }

    public void derivate(){
        Elemm el = head;

        el.setKf(el.getKf()+ el.getSt());
        el.setSt(el.getSt()-1);
        while (el.getNext() != null) {
            el.getNext().setKf(el.getNext().getKf()+ el.getNext().getSt());
            el.getNext().setSt(el.getNext().getSt()-1);
            el = el.getNext();
        }

    }

}
