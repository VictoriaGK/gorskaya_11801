import ru.itis.kpfu.lasttask.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;


public class Main {

    public static ArrayList<User> users = new ArrayList<User>();
    public static ArrayList<Channel> channels = new ArrayList<Channel>();
    public static ArrayList<Video> videos = new ArrayList<Video>();
    public static ArrayList<History> histories = new ArrayList<History>();
    public static ArrayList<Like<Video>> likes = new ArrayList<Like<Video>>();
    public static ArrayList<Post> posts = new ArrayList<Post>();
    public static ArrayList<Subscribe> subscribes = new ArrayList<Subscribe>();
    public static ArrayList<Like<Post>> postlikes = new ArrayList<Like<Post>>();
    public static ArrayList<Comment> comments = new ArrayList<>();
    public static ArrayList<Like<Comment>> commentslike = new ArrayList<Like<Comment>>();
    public static String s;
    public static String[] strings;
    public static Scanner sc;

    public static void main(String[] args) throws FileNotFoundException {
/* На данный момент Video и Post не сильно отличаются (вообще нет),
но в них есть разница.(появится) ( сделаем)...
Всё сделано с заделом на будущее. Можно будет легко добавлять методы в уже существуюшие классы ,чтобы делать запросы.
*/
        getUsers();
        getChannels();
        getVideos();
        getHistories();
        getVideoLikes();
        getPosts();
        getPostLikes();
        getSubscribers();
        getComments();
        getCommentsLikes();
        for (int i = 0; i < comments.size(); i++) {
            System.out.println(comments.get(i).toString());
        }
        System.out.println("Fuck you... That's why!");

        for(User user: users){
            if(Task3.channelExist(user, channels, 1,videos,likes )){
                System.out.println(user);
            }
        }





    }

    private static void getCommentsLikes() throws FileNotFoundException {
        User current = null;
        Comment c = null;
        sc = new Scanner(new File("LikeOfComment.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[0]);
            c = getCurrentComment(strings[1]);
            commentslike.add(new Like<Comment>(current, c, strings[2].equalsIgnoreCase("true")));
        }
    }
    private static void getComments() throws FileNotFoundException {
        User current = null;
        Video v = null;
        Comment c = null;
        sc = new Scanner(new File("CommentOfVideo.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[1]);
            v = getCurrentVideo(strings[4]);
            c = getCurrentComment(strings[5]);
            comments.add(new Comment(current, strings[2], strings[3], Integer.parseInt(strings[0]), c, v));
        }
        current = null;
        Post p = null;
        c = null;
        sc = new Scanner(new File("CommentofPost.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[1]);
            p = getCurrentPost(strings[4]);
            c = getCurrentComment(strings[5]);
            comments.add(new Comment(current, strings[2], strings[3], Integer.parseInt(strings[0]), c, p));
        }

    }

    private static Comment getCurrentComment(String string) {
        for (Comment c : comments) {
            if (Integer.parseInt(string) == c.getId()) {
                return c;
            }
        }
        return null;
    }

    private static void getSubscribers() throws FileNotFoundException {
        User current = null;
        Channel cur1 = null;
        sc = new Scanner(new File("Subscribe.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[0]);
            cur1 = getCurrentChannel(strings[1]);
            subscribes.add(new Subscribe(current, cur1));
        }
    }

    private static void getPostLikes() throws FileNotFoundException {
        User current = null;
        Post post = null;
        sc = new Scanner(new File("LikeOfPost.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[0]);
            post = getCurrentPost(strings[1]);
            postlikes.add(new Like<Post>(current, post, strings[2].equalsIgnoreCase("true")));
        }
    }

    private static Post getCurrentPost(String string) {
        for (Post p : posts) {
            if (Integer.parseInt(string) == p.getId()) {
                return p;
            }
        }
        return null;
    }

    private static void getPosts() throws FileNotFoundException {
        sc = new Scanner(new File("Post.txt"));
        Channel cur1 = null;
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            cur1 = getCurrentChannel(strings[0]);
            posts.add(new Post(Integer.parseInt(strings[4]), strings[2], cur1, strings[3], strings[1]));
        }
    }

    private static void getVideoLikes() throws FileNotFoundException {
        User current = null;
        Video video = null;
        sc = new Scanner(new File("LikeOfVideo.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[0]);
            video = getCurrentVideo(strings[1]);
            likes.add(new Like<Video>(current, video, strings[2].equalsIgnoreCase("true")));
        }
    }

    private static void getHistories() throws FileNotFoundException {
        sc = new Scanner(new File("History.txt"));
        User current = null;
        Video v = null;
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[0]);
            v = getCurrentVideo(strings[1]);
            histories.add(new History(current, v, strings[2]));
        }
    }

    private static Video getCurrentVideo(String string) {
        for (Video video : videos) {
            if (Integer.parseInt(string) == video.getId()) {
                return video;
            }
        }
        return null;
    }

    private static void getVideos() throws FileNotFoundException {
        sc = new Scanner(new File("Video.txt"));
        Channel cur1 = null;
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            cur1 = getCurrentChannel(strings[3]);
            videos.add(new Video(Integer.parseInt(strings[2]), strings[1], cur1, strings[4], strings[0]));
        }
    }

    private static void getChannels() throws FileNotFoundException {
        sc = new Scanner(new File("Channel.txt"));
        User current = null;
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            current = getCurrentUser(strings[2]);
            channels.add(new Channel(Integer.parseInt(strings[0]), strings[1], current, strings[3]));
        }
    }

    public static User getCurrentUser(String s) {
        for (User u : users) {
            if (Integer.parseInt(s) == u.getUserId()) {
                return u;
            }
        }
        return null;
    }

    public static Channel getCurrentChannel(String s) {
        for (Channel c : channels) {
            if (Integer.parseInt(s) == c.getChannel_id()) {
                return c;
            }
        }
        return null;
    }

    public static void getUsers() throws FileNotFoundException {
        sc = new Scanner(new File("User.txt"));
        while (sc.hasNextLine()) {
            s = sc.nextLine();
            strings = s.split("\t");
            users.add(new User(strings[0], strings[1], Integer.parseInt(strings[2]), strings[3]));
        }
    }






}


