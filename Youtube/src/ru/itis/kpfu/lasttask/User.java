package ru.itis.kpfu.lasttask;

public class User {
    private String username;
    private String dateCreation;
    private int userId;
    private String info;

    public User(String username, String dateCreation, int userId, String info) {
        this.username = username;
        this.dateCreation = dateCreation;
        this.userId = userId;
        this.info = info;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", dateCreation='" + dateCreation + '\'' +
                ", userId='" + userId + '\'' +
                ", info='" + info + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public int getUserId() {
        return userId;
    }

    public String getInfo() {
        return info;
    }
}
