package ru.itis.kpfu.lasttask;

/*
комент на комент
*/
public class Comment {
    private User user;
    private String info;
    private String date;
    private int id; //Integer

    public Comment getPrevious() {
        return previous;
    }

    private Comment previous;

    public Content getPlace() {
        return place;
    }

    private Content place;

    public Comment(User user, String info, String date, int id, Comment previous, Content place) {
        this.user = user;
        this.info = info;
        this.date = date;
        this.id = id;
        this.previous = previous;
        this.place = place;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getComment() {
        return info;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "user=" + user +
                ", info='" + info + '\'' +
                ", date='" + date + '\'';
    }
}
