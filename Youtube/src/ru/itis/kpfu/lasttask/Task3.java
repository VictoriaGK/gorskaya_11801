package ru.itis.kpfu.lasttask;

import java.util.ArrayList;

public class Task3 {
    public static boolean channelExist(User user, ArrayList<Channel> channels, int n, ArrayList<Video> videos, ArrayList<Like<Video>> likes){
        for (Channel channel: channels){
            if(channel.getOwner().equals(user)){
                if(videoExist(channel,n,videos, likes)){
                    return true;
                }
            }
        }
        return false;
    }
    public static boolean videoExist(Channel channel, int n, ArrayList<Video> videos,ArrayList<Like<Video>> likes){
        for(Video video: videos){
           if(video.getChannel().equals(channel)){
               return likeExist(likes,video,n);

            }
        }
        return false;
    }
    public static boolean likeExist(ArrayList<Like<Video>> likes, Video video, int n){
        int k = 0;
        for(Like like: likes){
            if(like.getPlace().equals(video)){
                k++;
            }
        }
        if (k>=n){
            return true;
        }
        return false;
    }
}
