package ru.itis.kpfu.lasttask;

public class Like<T> {
    private User user;
    private T place;
    private boolean res;

    public Like(User user, T place, boolean res) {
        this.user = user;
        this.place = place;
        this.res = res;
    }

    public User getUser() {
        return user;
    }

    public T getPlace() {
        return place;
    }

    public boolean isRes() {
        return res;
    }

    @Override
    public String toString() {
        return "Like{" +
                "user=" + user +
                ", place=" + place +
                ", res=" + res +
                '}';
    }
}
