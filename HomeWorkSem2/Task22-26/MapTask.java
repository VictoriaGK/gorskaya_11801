package ru.itis.kpfu;
import java.util.Map;
import java.util.TreeMap;

public class MapTask {

    public static Map<Character,Integer> stat(String text) {
        Map<Character,Integer> res = new TreeMap<>();
        for (int i = 0; i < 26; i++) {
            res.put((char) (i +97),0);
        }
        text = text.toLowerCase();
        for (int i = 0; i < text.length(); i++) {
            char ch = (char) text.charAt(i);
            res.put(ch,res.get(ch) + 1);
        }
        return res;
    }


    
}
