package ru.itis.kpfu;

import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.TreeMap;

public class PrefixPostfix {

    public static boolean isOperator(char c) {
        if (c == '+' || c == '-'
                || c == '*' || c == '/'
                || c == '^') {
            return true;
        }
        return false;
    }

        public static double eval(Node<String> tree, Map<String, Double> variables) {
        if (tree == null) {
            return 0;
        }

        if (tree.left == null && tree.right == null) {
            return variables.get(tree.value);
        }

        double leftRes = eval(tree.left, variables);

        double rightRes = eval(tree.right, variables);

        if ( tree.value.equals("+")) {
            return leftRes + rightRes;
        }
        if ( tree.value.equals("-")) {
            return leftRes - rightRes;
        }
        if ( tree.value.equals("*")) {
            return leftRes * rightRes;
        }
        if ( tree.value.equals("/")) {
            return leftRes / rightRes;
        }
        throw new IllegalStateException();
    }

    public static void inorder(Node<String> tree) {
        if (tree != null) {
            inorder(tree.left);
            System.out.println(tree.value + " ");
            inorder(tree.right);
        }
    }

    public static Node constructTreePostfix(String postfix) {
        Stack<Node> stack = new Stack<>();
        Node t, t1, t2;
        for ( int i = 0; i < postfix.length(); i++) {
            if (!isOperator(postfix.charAt(i))) {
                t = new Node();
                t.value = postfix.charAt(i);
                t.left = null;
                t.right = null;
                stack.push(t);
            } else {
                t = new Node();
                t.value = postfix.charAt(i);
                t.left = null;
                t.right = null;

                t1 = stack.pop();
                t2 = stack.pop();

                t.right = t1;
                t.left = t2;

                stack.push(t);

            }
        }
        t = stack.peek();
        stack.pop();
        return t;
    }
    static String string = "";
    public static void postorder(Node<String> tree) {
        if (tree != null) {
            postorder(tree.left);
            postorder(tree.right);
            string += tree.value;
        }
    }

    public static void printTree(Node p, int h) {
        if (p != null ){
            printTree(p.right, h + 1);
            for (int i = 0; i < h; i++) {
                System.out.print("  ");
            }
            System.out.println(p.value);
            printTree(p.left, h + 1);
        }
    }

}

