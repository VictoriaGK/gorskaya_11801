package ru.itis.kpfu;

import java.util.ArrayDeque;
import java.util.Queue;

public class Task18 {

    public static int task18(int n) {
        Queue<Integer> q2 = new ArrayDeque<>();
        Queue<Integer> q5 = new ArrayDeque<>();
        Queue<Integer> q3 = new ArrayDeque<>();
        int cur = 1;
        int count = 0;
        while (cur  < n) {
            q2.offer(cur * 2);
            q3.offer(cur * 3);
            q5.offer(cur * 5);
            cur = min(q2.element(), min(q3.element(), q5.element()));
            if (q5.element() == cur)
                q5.remove();
            if (q2.element() == cur)
                q2.remove();
            if (q3.element() == cur)
                q3.remove();
            count++;
        }

        return count;
    }

    public static int min (int a, int b) {
        return a < b ? a : b;
    }
}
