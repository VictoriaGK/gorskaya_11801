package ru.itis.kpfu;

public class HTMLParagraphSerializer extends Serializer {
    public HTMLParagraphSerializer() {
    }

    @Override
    String serialize(String[] s) {
        String[] ss = new String[s.length];


        for (int i = 0; i < s.length; i++) {
            s[i] = "<p>" + s[i] + "</p>";
        }


        String str = String.join("\n", s);
        return str;
    }
}
