package ru.itis.kpfu;

public abstract class Serializer {
    abstract String serialize(String[] s);
}
