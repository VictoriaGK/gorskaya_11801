package ru.itis.kpfu;

public class HTMLListSerializer extends Serializer {

    public HTMLListSerializer() {
    }

    @Override
    String serialize(String[] s) {
        String[] ss = new String[s.length];


        for (int i = 0; i < s.length; i++) {
            s[i] =  "\t"+"<li>"  + s[i] + "</li>";
        }

        String str = String.join("\n", s);
        str = "<ul>" + "\n" + str + "\n" + "</ul>" ;
        return str;
    }
}
