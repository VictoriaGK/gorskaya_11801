package ru.itis.kpfu;

public class Elem {
    int value;

    Elem next;

    public int getValue() {
        return value;
    }

    public Elem() {
        this.value = 0;
        this.next = null;


    }
    public Elem getNext() {
        return next;
    }

    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    @Override
    public String toString() {
        return "Elem{" +
                "value=" + value +
                ", next=" + next +
                '}';
    }
    public static Elem  deleteHead(Elem p){
        p = p.next;
        return p;
    }



    public  static Elem deleteLast(Elem h){

        while (h.next!= null ){
            if(h.next.next == null){
                h.next = null;
                return h;

            } else{
                deleteLast(h.next);
                return h;
            }
        }
        h = null;
        return h;

    }

    public static Elem deleteLastLast(Elem h){
        if(h.next == null){
            return h = null;
        }
        while (h.next.next!= null ){
            if(h.next.next.next == null){
                h.next = h.next.next;
                return h;

            } else{
                deleteLastLast(h.next);
                return h;
            }
        }
        h = h.next;
        return h;

    }

    public static Elem removeK(Elem p, int k) {

        while (p != null) {

            if (p.value == k) {
                p = p.next;
                return p;
            } else {
                p.next = removeK(p.next, k);
                return p;

            }
    }

        return p;
    }



    public static Elem removeKAll(Elem p, int k) {
        Elem h  = p;

        while (h != null) {


            if (h.value == k) {
                p = p.next;



            } else {

                p.next = removeKAll(p.next, k);

            }

            h = h.next;

        }




        return p;
    }

    public static Elem insert(Elem p, int m, int k){
        if (p == null){
            return p;
        }

        if(p.value == k){
            Elem h = new Elem(m, p);


            Elem p2 = new Elem(m, p.next);
            p.next = p2;
            return h;
        }

        if(p.next.value==k){
            Elem h = new Elem(m, p.next);
            p.next=h;

            Elem p2 = new Elem(m, h.next.next);
            h.next.next=p2;
        }
        else insert(p.next,m,k);
        return p;
    }



    public static int max(Elem p){
        int max = p.value;
        while (p!= null) {
            // System.out.println(p.value);

            if(p.value> max){

                max = p.value;
            }
            p = p.next;

        }return max;
    }

    public static int sum(Elem p){
        int sum = 0;
        while (p!= null) {
            // System.out.println(p.value);

            sum = sum + p.value;
            p = p.next;

        }return sum;



    }

    public static boolean isNegative(Elem p){

        while (p!=null){
            if (p.value <0){
                return true;
            }
            else{
                p = p.next;
            }

        }
        return false;

    }

}
