package ru.itis.kpfu;

import java.util.ArrayList;
import java.util.Comparator;

public class Task13 {
    public static void task13

    {


        ArrayList<Student> mst = new ArrayList<Student>();
        mst.add(new Student("lulu", 4567, "omdsk", 99));
        mst.add(new Student("kit", 3444, "piter", 98));
        mst.add(new Student("aitt", 9090, "gissen", 96));
        Comparator<Student> comparator = (o1, o2) -> o1.getName().compareTo(o2.getName());


        Comparator comparator1 = new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return (o1.getAverageScore().compareTo(o2.getAverageScore()));
            }
        };

        class MyComparator2 implements Comparator<Student> {
            public int compare(Student o1, Student o2) {
                return o1.getCity().compareTo(o2.getCity());
            }
        }

        mst.sort(new Comparator<Student>() {
            @Override
            public int compare(Student o1, Student o2) {
                return (o1.getYear().compareTo(o2.getYear()));
            }
        });
    }
}
