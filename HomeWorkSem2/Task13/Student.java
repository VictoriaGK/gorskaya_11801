package ru.itis.kpfu;

public class Student implements Comparable<Student>{
    @Override
    public int compareTo(Student o) {
        return getName().compareTo(o.name);
    }

    private String name;
    private Integer year;
    private String city;
    private Integer averageScore;

    public Student(String name, Integer year, String city, Integer averageScore) {
        this.name = name;
        this.year = year;
        this.city = city;
        this.averageScore = averageScore;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setAverageScore(Integer averageScore) {
        this.averageScore = averageScore;
    }

    public String getName() {
        return name;
    }

    public Integer getYear() {
        return year;
    }

    public String getCity() {
        return city;
    }

    public Integer getAverageScore() {
        return averageScore;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", year=" + year +
                ", city='" + city + '\'' +
                '}';
    }
}
