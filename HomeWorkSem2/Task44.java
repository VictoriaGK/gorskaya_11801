package ru.itis.kpfu;
import java.util.HashMap;
import java.util.Map;

public class Task44 {
    public static int calc(int n) {
        Map<Integer, Integer> count = new HashMap<>();
        count.put(1, 1); count.put(3, 1); count.put(2, 1);
        if (count.containsKey(n))
            return count.get(n);
        else {
            if (n == 1)
                return 1;
            else if (n == 2)
                return 1;
            else if (n == 3)
                return 1;
            else {
                int r = calc(n - 1);
                if(n % 2 == 0)
                    r += calc(n / 2);
                if(n % 3 == 0)
                    r += calc(n / 3);
                count.put(n, r);
                return r;
            }
        }
    }

}
