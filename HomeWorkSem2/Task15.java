package ru.itis.kpfu;

import java.util.Stack;

public class Task15 {

    public static void task15(String s){
        int count = 0;
        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == '('){
                count++;

            }
            if (c==')'){
                count--;
            }
            if(count<0){
                System.out.println("More ')' ");
                break;
            }

        }
        if (count>0){
            System.out.println("More '(' ");
        }

    }

    public static  void   task151(String s){
        String open = "([{<";
        String close = ")]}>";
        Stack st = new Stack ();

        for(int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if(open.contains(Character.toString(c))){
                st.push(c);
            }else{
                if(close.contains(Character.toString(c))){
                    int j = 0;
                    while (close.charAt(j)!=c){
                        j++;
                    }


                    if(st.empty()){
                        System.out.println("More ')}]>' ");
                    }
                    char er = (char)st.peek();
                    if(!st.pop().equals(open.charAt(j))){
                        System.out.println("error" + er);
                    }


                }
            }
        }


    }

}
