package ru.itis.kpfu;

import java.util.ArrayList;
public class Task42 {
    public static ArrayList<Boolean> seating(int n, int k) {
        ArrayList<Boolean> place = new ArrayList<>();
        if (n - k < 2 || k < (n - 2) / 3) {
            System.out.println("false");
            return null;
        }
        int m = n - k;
        while (m > 2 && k > 0) {
            place.add(true);
            place.add(true);
            place.add(false);
            m -= 2;
            k--;
        }
        place.add(true);
        place.add(true);
        if(k > 0) {
            place.set(place.size() - 1, false);
            k--;
            while (k > 0) {
                place.add(false);
                k--;
            }
            place.add(true);
        }
        return place;
    }

}
