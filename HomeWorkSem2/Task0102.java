package ru.itis.kpfu;

import java.util.Scanner;

public class Task0102 {
    public static Elem method(Elem p,Elem head){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        for (int i = 0; i <n ; i++){
            p = new Elem();

            p.value = sc.nextInt();


            p.next = head;


            head = p;

        }
        return p;

    }
    public static void methodWriter(Elem p, int n) {
        Elem head = null;
        Elem p1 = null;

        for (int i = 0; i < n; i++) {
            p1 = new Elem();

            p1.value = p.value;
            p = p.next;


            p1.next = head;


            head = p1;

        }


        while (p1 != null) {
            System.out.println(p1.value);
            p1 = p1.next;
        }
    }

}
