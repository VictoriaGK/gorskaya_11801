package ru.itis.kpfu;

import java.util.*;

public class DoubleStackQueue<T> implements Queue<T> {
    Stack<T> stack = new Stack<>();
    Stack<T> stack2 = new Stack<>();

    @Override
    public int size() {
        return stack.size();
    }

    @Override
    public boolean isEmpty() {
        return stack.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return stack.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return stack.iterator();
    }

    @Override
    public Object[] toArray() {
        return stack.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        stack.push(t);
        return true;
    }

    @Override
    public boolean remove(Object o)0 {
        stack.remove(o);
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return stack.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T t : c) {
            stack.push(t);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        stack.retainAll(c);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return stack.retainAll(c);
    }

    @Override
    public void clear() {
        stack.clear();
        return;
    }

    @Override
    public boolean offer(T t) {
        stack.push(t);
        return true;
    }

    @Override
    public T remove() {
        if (stack.size() == 0) throw new NoSuchElementException();

        for (T t : stack) {
            stack2.push(t);
        }
        T x = stack2.pop();
        for (T t : stack2) {
            stack.push(t);
        }
        return x;
    }

    @Override
    public T poll() {
        for (T t : stack) {
            stack2.push(t);
        }
        T x = stack2.pop();
        for (T t : stack2) {
            stack.push(t);
        }
        return x;
    }

    @Override
    public T element() {
        if (stack.size() == 0) throw new NoSuchElementException();
        for (T t : stack) {
            stack2.push(t);
        }
        T x = stack2.peek();
        for (T t : stack2) {
            stack.push(t);
        }
        return x;
    }

    @Override
    public T peek() {
        for (T t : stack) {
            stack2.push(t);
        }
        T x = stack2.peek();
        for (T t : stack2) {
            stack.push(t);
        }
        return x;
    }
}
