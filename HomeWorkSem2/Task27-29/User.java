import java.util.ArrayList;

public class User {
    private int id;
    private String username;
    private String password;
    private Gender gender;
    private String mail;
    private int age;
    private String city;

    public User(int id, String username, String password, Gender gender, String mail, int age) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.mail = mail;
        this.age = age;
    }

    public User(int id, String username, String password, Gender gender, String mail, int age, String city) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.mail = mail;
        this.age = age;
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Object[] friends(ArrayList<User> users) {
        return users.stream().filter(y -> Main2.isFriends(this, y)).toArray();
    }
    public Object[] city(ArrayList<User> users, ArrayList<Subscriber> subscribers) {
        return users.stream().filter(y -> Main2.isCity(this,subscribers)).toArray();
    }


    public long inMessages(ArrayList<Message> messages) {
        return messages.stream().filter(y -> y.getTo().equals(this)).count();
    }


}
