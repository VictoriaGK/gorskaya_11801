public class Sub {
    private User subscriber_id;
    private User subscription_id;

    public Sub(User subscriber_id, User subscription_id) {
        this.subscriber_id = subscriber_id;
        this.subscription_id = subscription_id;
    }
    public Sub(){}
    public User getSubscriber_id() {
        return subscriber_id;
    }

    public User getSubscription_id() {
        return subscription_id;
    }

    public void setSubscriber_id(User subscriber_id) {
        this.subscriber_id = subscriber_id;
    }

    public void setSubscription_id(User subscription_id) {
        this.subscription_id = subscription_id;
    }

    @Override
    public String toString() {
        return "Sub{" +
                "subscriber_id=" + subscriber_id +
                ", subscription_id=" + subscription_id +
                '}';
    }
}
