import java.util.Scanner;

public class Message {
    private User from;
    private User to;
    private String time;
    private String text;
    private Status status;

    public Message(User from, User to, String time, String text, Status status) {
        this.from = from;
        this.to = to;
        this.time = time;
        this.text = text;
        this.status = status;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}

