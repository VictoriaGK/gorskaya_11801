import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Task2 {


    public static Map<User, ArrayList<User>> task3(ArrayList<Sub> s, ArrayList<User> users){
        Map<User, ArrayList<User>> hashMap = new HashMap<User, ArrayList<User>>();
        for(User user: users) {
            ArrayList<User> u = new ArrayList<>();

            for (Sub sub : s) {
                if (sub.getSubscriber_id().equals(user)) {

                    for (Sub sub1 : s) {
                        if ((sub1.getSubscriber_id().equals(sub.getSubscription_id())) &&( sub1.getSubscription_id().equals(user))) {
                            u.add(sub.getSubscription_id());
                        }
                    }
                }

            }
            hashMap.put(user, u);

        }

        return hashMap;
    }


}
