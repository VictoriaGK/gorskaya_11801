import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main2 {
    public static Scanner sc;
    public static String s;
    public static String[] strings;
    public static ArrayList<User> users = new ArrayList<User>();
    public static ArrayList<Message> messages = new ArrayList<Message>();
    public static ArrayList<Subscriber> subscribers = new ArrayList<Subscriber>();

    public static void main(String[] args) throws FileNotFoundException {
        parseData("users.txt", "sub.txt", "messages.txt");
        writeFriends();
        System.out.println();
        System.out.println(Arrays.toString(oldAlone()));
    }

    public static void parseData(String path, String path2, String path3) throws FileNotFoundException {
        sc = new Scanner(new File(path));
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            User user = new User(Integer.parseInt(temp[0]), temp[1], temp[2], temp[3].equals(1) ?
                    Gender.Male : Gender.Female, temp[4], Integer.parseInt(temp[5]));
            users.add(user);
        }
        sc = new Scanner(new File(path2));
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Subscriber subscriber = new Subscriber(findUserById(Integer.parseInt(temp[0])), findUserById(Integer.parseInt(temp[1])));
            subscribers.add(subscriber);
        }
        sc = new Scanner(new File(path3));
        while (sc.hasNextLine()) {
            String[] temp = sc.nextLine().split("\t");
            Message message = new Message(findUserById(Integer.parseInt(temp[0])), findUserById(Integer.parseInt(temp[1])),
                    temp[2], temp[3], Status.valueOf(temp[4]).equals(Status.checked) ? Status.checked : Status.unchecked);
            messages.add(message);
        }
    }

    public static User findUserById(int id) {
        for (User user: users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public static boolean isFriends(User user1, User user2) {
        Subscriber subscriber1 = new Subscriber(user1, user2);
        Subscriber subscriber2 = new Subscriber(user2, user1);
        if (subscribers.contains(subscriber1) &&
                subscribers.contains(subscriber2)) {
            return true;
        }
        return false;
    }
    public static boolean isCity(User user1, ArrayList<Subscriber> subscriber1) {

        String city = null;
        for(Subscriber subscriber: subscribers){
            if(subscriber.getFrom().equals(user1)){
                if(!(city.equals(null)||subscriber.getTo().getCity().equals(city))){
                    return false;
                }else{
                    if(city.equals(null)){
                        city = subscriber.getTo().getCity();
                    }
                }

            }
        }
        return true;
    }


    public static void writeFriends() {
        for (User user: users) {
            Object[] temp = user.friends(users);
            String tempString = user.getUsername() + ": ";
            for (Object object: temp) {
                User tempUser = (User) object;
                tempString += tempUser.getUsername() + ", ";
            }
            System.out.println(tempString);
        }
    }


    public static Object[] oldAlone() {
        return users.stream().filter(y -> y.friends(users).length == 0 && y.getAge() > 17).map(User::getUsername).toArray();
    }



}

