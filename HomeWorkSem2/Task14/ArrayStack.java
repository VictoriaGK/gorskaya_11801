package ru.itis.kpfu;

import java.util.ArrayList;

public class ArrayStack<T> implements IStack<T> {

    ArrayList<T> arrayList = new ArrayList<>();


    @Override
    public void push(T elem) {
        arrayList.add(elem);
    }

    @Override
    public T pop() {
        T ob = arrayList.get(arrayList.size() - 1);
        arrayList.remove(arrayList.size() - 1);
        return ob;
    }

    @Override
    public T peek() {
        return arrayList.get(arrayList.size() - 1);
    }

    @Override
    public boolean isEmpty() {
        return arrayList.isEmpty();
    }
}
