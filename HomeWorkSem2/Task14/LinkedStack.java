package ru.itis.kpfu;

public class LinkedStack<T> implements IStack<T> {

    private Node<T> head;
    private Node<T> last;

    @Override
    public void push(T elem) {
        last.next = new Node(elem);
    }

    @Override
    public T pop() {
        T value = last.value;
        last = null;
        return value;
    }

    @Override
    public T peek() {
        return last.value;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }
}
