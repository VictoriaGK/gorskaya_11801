package ru.itis.kpfu;

import java.util.*;
import java.util.ListIterator;

public class MyArraylist<E> extends ArrayCollection<E> implements  List<E> {

    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    protected int size = 0;
    protected int capacity;
    protected final int CAPACITY = 16;
    protected E[] arr;


    public MyArraylist() {
        capacity = CAPACITY;
        arr = (E[]) new Object[capacity];
    }

    public MyArraylist(int size, E[] array, E[] arr) {
        super(size, array);
        this.arr = arr;
    }

    public MyArraylist(int capacity) {
        this.capacity = capacity;
        arr = (E[]) new Object[capacity];
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }


    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(arr[i])) {
                return true;
            }
        }
        return false;
    }


//    @Override
//    public Object[] toArray() {
//        Object[] newArray = new Object[size];
//        System.arraycopy(arr, 0, newArray, 0, size);
//        return newArray;
//    }
//
//    @Override
//    public <T> T[] toArray(T[] a) {
//        return null;
//    }


    public boolean add(E element) {
        if (size >= capacity) {
            capacity = capacity * 2;
            Object[] newArr = new Object[capacity];
            for (int i = 0; i < size; i++) {
                newArr[i] = arr[i];
            }
            arr = (E[]) newArr;
        }
        arr[size] = element;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);
        E res = get(index);
        E[] newArr = arr;

        for (int i = index; i < size - 1; i++) {
            newArr[i] = arr[i + 1];
        }
        size--;

        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return super.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return super.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return super.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return super.retainAll(c);
    }


    public void add(int index, E element) {

        if (size < capacity) {
            for (int i = index; i < size - 1; i++) {
                arr[i + 1] = arr[i];
            }
            arr[index] = element;
            size++;


        } else {
            E[] newArr = (E[]) new Object[capacity * 2 + 1];
            for (int i = 0; i < index; i++) {
                newArr[i] = arr[i];
            }

            newArr[index] = element;
            for (int i = index; i < size; i++) {
                newArr[i + 1] = arr[i];

            }
            arr = newArr;
            size++;


        }

    }

    public void clear() {
        for (int i = 0; i < size; i++)
            arr[i] = null;
        size = 0;
    }


    public E get(int i) {
        if (i >= size)
            return null;
        return arr[i];
    }


    public E set(int index, E element) {
        return null;
    }

    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size; i > -1; i--) {
            if (o == arr[i]) {
                return i;
            }
        }
        return -1;
    }


    public E remove(int index) {

        E res = get(index);
        E[] newArr = arr;

        for (int i = index; i < size - 1; i++) {
            newArr[i] = arr[i + 1];
        }
        size--;

        return res;
    }


    public Iterator<E> iterator() {
        return new ArrayListIterator<>(arr);
    }


    private class ArrayListIterator<E> implements java.util.Iterator<E> {

        private int current = 0;
        private E[] values;
        ArrayListIterator(E[] values){
            this.values = values;
        }
        @Override
        public boolean hasNext() {
            return current < size;
        }
        @Override
        public E next() {
            if (!hasNext()) throw new java.util.NoSuchElementException();
            return values[current++];
        }
        @Override
        public void remove() {
            MyArraylist.this.remove(--current);
        }
    }


    @Override
    public ListIterator<E> listIterator() {
        return new ListIterator<E>() {
            private int cursor = 0;

            @Override
            public boolean hasNext() {
                return cursor < size;
            }

            @Override
            public E next() {
                return arr[cursor++];
            }

            @Override
            public boolean hasPrevious() {
                return cursor != 0;
            }

            @Override
            public E previous() {
                return (E) arr[cursor--];
            }

            @Override
            public int nextIndex() {
                return cursor + 1;
            }

            @Override
            public int previousIndex() {
                return cursor - 1;
            }

            @Override
            public void remove() {
                size--;
            }

            @Override
            public void set(E e) {
                arr[size - 1] = e;
            }

            @Override
            public void add(E e) {
                arr[size] = e;
                size++;
            }
        };
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return new ArrayList<E>(Arrays.copyOfRange(arr, fromIndex, toIndex));
    }





}