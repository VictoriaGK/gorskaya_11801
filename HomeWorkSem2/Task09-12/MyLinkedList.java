package ru.itis.kpfu;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyLinkedList<T> extends ListCollection<T> implements List<T> {
    protected Node<T> head;
    protected int size = 0;
    protected Node<T> last;


    @Override
    public T get(int index) {
        Node<T> current = head;
        if (size < index) {
        }
        int i = 0;
        while (index < i) {
            i++;
            current = current.getNext();
        }
        return (T)current.getNext().getValue();
    }

    public T set(int index, T element) {
        Node<T> current = head;
        if (size < index) {
        }
        int i = 0;
        while (index < i) {
            i++;
        }
        current.setValue(element);
        return current.getValue();
    }

    @Override
    public void add(int index, T element) {
        Node<T> current = head;
        for (int i = 0; i < index; i++) {
            current = head.getNext();
        }
        Node next = current.getNext();
        current.setNext(new Node<T>(element));
        current.getNext().setNext(current.getNext());
        size++;
    }

    @Override
    public T remove(int index) {
        Node<T> current = head;
        for (int i = 0; i < index - 1; i++) {
            current = head.getNext();
        }
        current.setNext(current.getNext().getNext());
        size--;
        return current.getValue();
    }


    @Override
    public int indexOf(Object o) {
        Node<T> current = head;
        int i = 0;
        while (current.getNext() != null) {
            i++;
            if (current.getValue().equals(o)) {
                return i;
            }
            current = current.getNext();
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<T> current = head;
        int i = 0;
        int x = -1;
        while (current.getNext() != null) {
            i++;
            if (current.getValue().equals(o)) {
                x = i;
            }
            current = current.getNext();
        }
        return x;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return null;
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return (size == 0);
    }


    @Override
    public T[] toArray() {
        Object[] objects = new Object[size];
        int i = 0;
        for (T node: this) {
            objects[i++] = node;
        }
        return (T[])objects;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        Node<T> current = head;
        for (int i = 0; i < index; i++) {
            current = head.getNext();
        }
        Node<T> next = current.getNext();
        Node<T> x;
        for (T t : c) {
            x = new Node(t);
            current.setNext(x);
            current = x;
        }
        current.setNext(next);
        size += c.size();
        return true;
    }


}
