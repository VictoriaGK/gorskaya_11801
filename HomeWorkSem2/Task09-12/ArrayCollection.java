package ru.itis.kpfu;

import java.util.Collection;
import java.util.Iterator;

public class ArrayCollection<T> implements Collection<T> {
    protected int size;
    protected T[] array;

    public ArrayCollection(int size, T[] array) {
        this.size = size;
        this.array = array;
    }

    public ArrayCollection() {
        array = (T[])(new Object[10]);
        size = 0;
    }
    public ArrayCollection(int length) {
        array = (T[])(new Object[length]);
        size = 0;
    }

    @Override
    public int size() {
        return  size;
    }




    @Override
    public boolean isEmpty() {
        return size()!=0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i <array.length ; i++) {
            if (array[i].equals(o)) return true;
        }
        return false;
    }



    @Override
    public boolean add(T t) {

        if(this.size() < this.array.length){
            array[this.size()] = t;
            size++;

        }else {
            T[] a = (T[]) new Object[this.array.length*2+1];
            for (int i = 0; i <this.size; i++) {
                a[i] = array[i];

            }
            this.array = a;
            array[size()] = t;
            size++;


        }


        return true;
    }




    @Override
    public Iterator<T> iterator(){
        Iterator<T> iter = new Iterator<T>() {
            int i = 0;
            @Override
            public boolean hasNext() {
                if (i < size()) {
                    return true;
                }
                return false;
            }

            @Override
            public T next() {
                return (T) array[i++];
            }

            @Override
            public void remove() {
                ArrayCollection.this.remove(array[i]);
            }
        };
        return iter;

    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i <this.size ; i++) {
            if (array[i].equals(o)){
                for (int j = i; j <this.size-1; j++) {
                    array[j]=array[j+1];
                }
                size--;
            }return true;

        }

        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        for (Object e: c){
            if(!contains(e)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T e: c){
            add(e);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object e: c){
            if (!contains(e)){
                remove(e);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean flag = false;
        for (Object e: c){
            if (contains(e)){
                remove(e);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public void clear() {
        for (int i = 0; i <size ; i++) {
            array[i] = null;
            
        }
        size = 0;

    }


    public String toString() {
        Iterator<T> it = iterator();
        if (! it.hasNext())
            return "[]";

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (;;) {
            T t = it.next();
            sb.append(t == this ? "(this Collection)" : t);
            if (! it.hasNext())
                return sb.append(']').toString();
            sb.append(',').append(' ');
        }
    }

    public void setSize(int size) {
        this.size = size;
    }
}
