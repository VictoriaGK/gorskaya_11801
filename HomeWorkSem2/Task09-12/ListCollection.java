package ru.itis.kpfu;

import java.util.Collection;
import java.util.Iterator;


public class ListCollection<T> implements Collection<T> {

    protected Node<T> node;// reference to the front of this collection
    protected int numElements; // number of elements in this collection
    protected Node<T> location;



    public boolean add(T element)
    // Adds element to this collection.
    {
        Node<T> newNode = new Node<T>(element, node);

        node = newNode;
        numElements++;
        return true;
    }


    @Override
    public int size() {
        return numElements;
    }

    @Override
    public boolean isEmpty() {
        return size()==0;
    }

    @Override
    public boolean contains(Object o) {
        location = node;


        while (location != null)
        {
            if (location.getValue().equals(o))  // if they match
            {
                return true;
            }
            else
            {
                location = location.getNext();
            }
        }

        return false;
    }

    @Override
    public Iterator<T> iterator(){
        Iterator<T> iter = new Iterator<T>() {
            Node n = node;
            @Override
            public boolean hasNext() {
                if (n.getNext()!= null) {
                    return true;
                }
                return false;
            }

            @Override
            public T next() {

                if(!hasNext()){
                    throw new java.util.NoSuchElementException();
                }
                return (T) n.getNext().getValue();

            }

//            @Override
//            public void remove() {
//                ArrayCollection.this.remove(array[i]);
//            }
        };
        return iter;

    }

    @Override
    public Object[] toArray() {
        Object[] arr = new Object[numElements];
        for (int i = 0; i <numElements ; i++) {
            arr[i]= node.getValue();
            node = node.getNext();

        }
        return arr;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean remove(Object o) {

        if(node.getValue().equals(o)){
            node = null;
            numElements--;
            return true;
        }
        while (node.getNext()!=null){
            if(node.getNext().getValue().equals(o)){
                node.setNext(node.getNext().getNext());
                numElements--;
                return true;
            }

        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object e: c){
            if(!contains(e)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T e: c){
            add(e);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object e: c){
            if (!contains(e)){
                remove(e);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean flag = false;
        for (Object e: c){
            if (contains(e)){
                remove(e);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public void clear() {
        while (node!=null){
            node = node.getNext();
        }
        numElements = 0;
    }
}
