package ru.itis.kpfu;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class Tree<T> {
    Node root;

    public static void createTree(Node p, int n) {
        p.value = n;
        int nl = n/2;
        int nr = n - 1 - nl;
        if (nl > 0) {
            p.left = new Node();
            createTree(p.left,nl);
        }
        if (nr > 0) {
            p.right = new Node();
            createTree(p.right,nr);
        }
    }

    public static void printTree(Node p, int h) {
        if (p != null ){
            printTree(p.right, h + 1);
            for (int i = 0; i < h; i++) {
                System.out.print("  ");
            }
            System.out.println(p.value);
            printTree(p.left, h + 1);
        }
    }

    public static int sumBFSearchLR(Node root) {
        Queue<Node> q = new LinkedList<>();
        q.offer(root);
        int sum = 0;
        while (!q.isEmpty()) {
            Node p = q.poll();
            sum += p.value;
            if (p.left != null) {
                q.offer(p.left);
            }
            if (p.right != null) {
                q.offer(p.right);
            }
        }
        return sum;
    }
    static int static_counter = 0;
    public static void parseDFSearchLCR(Node p) {
        if (p != null) {
            parseDFSearchLCR(p.left);
            p.value = static_counter++;
            parseDFSearchLCR(p.right);
        }
    }

    static int static_counter1 = 0;
    public static void parseDFSearchCLR(Node p) {
        if (p != null) {
            p.value = static_counter1++;
            parseDFSearchCLR(p.left);
            parseDFSearchCLR(p.right);
        }
    }

    static int static_counter2 = 0;
    public static void parseDFSearchLRC(Node p) {
        if (p != null) {
            parseDFSearchLRC(p.left);
            parseDFSearchLRC(p.right);
            p.value = static_counter2++;
        }
    }

    public static int sumTree(Node p) {
        if (p == null) {
            return 0;
        } else {
            return p.value + sumTree(p.left) + sumTree(p.right);
        }
    }

    public static int sumTreeIterative(Node p) {
        if ( p == null) {
            return 0;
        }
        Stack<Node> nodeStack = new Stack<>();
        nodeStack.push(p);
        int sum = 0;
        while (!nodeStack.empty()) {
            Node leaf = nodeStack.peek();
            sum += leaf.value;
            nodeStack.pop();
            if (leaf.right != null) {
                nodeStack.push(leaf.right);
            }
            if (leaf.left != null) {
                nodeStack.push(leaf.left);
            }
        }
        return sum;
    }

    
}
