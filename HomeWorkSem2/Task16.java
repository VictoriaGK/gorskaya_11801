package ru.itis.kpfu;

import java.util.Stack;

public class Task16 {
    public static double task16(String s){
        String op = "+-*/";
        Stack st = new Stack ();
        double result = 0;
        double b = 0;
        String [] mas = s.split(" ");


        for (int i = 0; i <mas.length ; i++) {
            String c = mas[i];
            if (op.contains(c)){

                result = (double) st.pop();
                b = (double) st.pop();
                switch (c){
                    case "+":  result = result+b;
                    break;
                    case "-":  result = b-result;
                    break;
                    case "*":  result = result*b;
                    break;
                    case "/":  result = b/result;
                    break;
                }
                st.push(result);

            }else{
                st.push(Double.parseDouble(c));
            }


        }
        return result;


    }
}
