package ru.itis.kpfu;

import java.io.*;
import java.net.URL;

public class MyThread2 extends Thread{
    String url;
    File file;
    //

    public MyThread2() {
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public MyThread2(String url, File file) {
        this.url = url;
        this.file = file;
    }

    public void run(){
        try {
            Main.dowmloadJpg(url, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
