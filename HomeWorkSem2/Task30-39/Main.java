package ru.itis.kpfu;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static  void genBinaryFile(String filename, byte [] bytes)throws IOException {
        int n = bytes.length;
        FileOutputStream fos = new FileOutputStream(filename);

        fos.write(bytes);
        fos.close();
    }
    //29
    public static  void copyFile(String str, String dest)throws IOException {
        FileInputStream fis = new FileInputStream(str);
        FileOutputStream fos = new FileOutputStream(dest);
        while (fis.available()!=0){
            fos.write(fis.read());
        }
        fis.close();
        fos.close();
    }
    public static byte[] readBytes(String filename, int len ) throws IOException {
        byte[] bytes = new byte[len];
        FileInputStream fis = new FileInputStream(filename);
        int k = fis.read(bytes);
        if(k!= len){
            throw  new ArrayIndexOutOfBoundsException("not the same number");
        }
        return bytes;
    }

//30
    public static void copyFile(File file1, File file2) throws FileNotFoundException {

        Reader r = new BufferedReader(new InputStreamReader(new FileInputStream(file1)));
        PrintWriter pr = new PrintWriter(file2);

    }

    //31
    public static void dowmloadJpg(String url1, File file1) throws IOException {
        URL url = new URL(url1);
        InputStream inputStream = url.openStream();
        FileOutputStream outputStream = new FileOutputStream(file1);
        int k = inputStream.read();
        while (k != -1) {
            outputStream.write(k);
            k = inputStream.read();
        }
        inputStream.close();
        outputStream.close();
    }
    //32

    public static void downloadSite(URL url, File file) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
        PrintWriter printWriter = new PrintWriter(file);
        String k = reader.readLine();
        while (k != null) {
            printWriter.write(k + "\n");
            k = reader.readLine();
        }
        reader.close();
        printWriter.close();

    }
    //33

    public static void downloadType(URL url, String type) throws IOException {
        File file = new File("stranica.html");
        downloadSite(url, file);
        int index = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String string = reader.readLine();
        while (string != null) {
            String s = findType(string, type);
            if (s.length() > 0) {
                System.out.println(s);
                index++;
                try {
                    dowmloadJpg(s, new File(index < 10 ? type + "0" + index + "." + type : type + index + "." + type));
                } catch (IOException e) {
                    string = reader.readLine();
                    continue;
                }
            }
            string = reader.readLine();
        }
    }


    public static String findType(String string, String type) {
        string = string.toLowerCase();
        String pattern = "\"http[^\"]*/[^\"]*\\." + type + "\"";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        m.find();
        try {
            String temp = m.group();
            return temp.substring(1, temp.length() - 1);
        } catch (IllegalStateException e) {
            return "";
        }
    }
    //36
    public static void copyFiles(File file, int n) {
        Mythread3[] myThreads = new Mythread3[n];
        for (int i = 0; i < n; i++) {
            myThreads[i] = new Mythread3(file, new File("copyFile" + i + ".txt"));
        }
        for (Mythread3 myThread: myThreads) {
            myThread.start();
        }
    }


    //37
    public static void copyFiles(String file,  int n, int k) throws InterruptedException {
        int j = 0;

        Mythread3[] myThreads = new Mythread3[k];
        for (int i = 0; i <k ; i++) {
            myThreads[j] = new Mythread3();

        }
        while (j<n){

            for (int i = 0; i < k; i++) {
                if (!(myThreads[i].isAlive())) {
                    myThreads[i] = new Mythread3(file, new String("file" + j + ".txt"));

                    myThreads[i].start();

                    break;

                } else {
                    myThreads[i].join();
                    myThreads[i] = new Mythread3(file, new String("file" + j + ".txt"));

                    myThreads[i].start();
                    break;
                }

            }
            j++;

        }


    }
    //39
    public static void downloadsThread(String url1, String type, int k) throws IOException, InterruptedException {
        URL url = new URL(url1);
        File file = new File("stranica.html");
        downloadSite(url, file);
        int index = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String string = reader.readLine();
        MyThread2[] myThreads = new MyThread2[k];
        for (int j = 0; j <k ; j++) {
            myThreads[j] = new MyThread2();

        }
        while (string != null) {
            String s = findType(string, type);
            if (s.length() > 0) {
                System.out.println(s);
                index++;
                for (int i = 0; i < k; i++) {
                    if(!(myThreads[i].isAlive())){
                        myThreads[i] = new MyThread2(s,new File(index < 10 ? type + "0" + index + "." + type : type + index + "." + type));

                        myThreads[i].start();

                        break;

                    }else {
                        myThreads[i].join();
                        myThreads[i] = new MyThread2(s,new File(index < 10 ? type + "0" + index + "." + type : type + index + "." + type));

                        myThreads[i].start();
                        break;
                    }


                }

            }
            string = reader.readLine();
        }

    }
//38
    public static void downloadsThread1(String url1, String type) throws IOException, InterruptedException {
        URL url = new URL(url1);
        File file = new File("stranica.html");
        downloadSite(url, file);
        int index = 0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        String string = reader.readLine();

        while (string != null) {
            String s = findType(string, type);
            if (s.length() > 0) {
                System.out.println(s);
                index++;
                MyThread2 m = new MyThread2();
                m.start();
            }
            string = reader.readLine();
        }

    }


}
