package ru.itis.kpfu;

import java.io.File;
import java.io.IOException;

public class Mythread3 extends Thread {
    String file;
    String file1;

    public Mythread3(String file, String file1) {
        this.file = file;
        this.file1 = file1;
    }

    public Mythread3() {
    }

    public void run(){
        try {
            Main.copyFile(file, file1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
