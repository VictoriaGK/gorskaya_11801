package ru.itis.kpfu;

import java.util.*;

public class ArrayQueue<T> implements Queue<T> {

    private int size;
    T[] objects;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (T t : objects) {
            if (t.equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> iterator = new Iterator<>() {
            private int index = 0;
            private Object objects1[] = objects;

            @Override
            public boolean hasNext() {
                return next() != null;
            }

            @Override
            public T next() {
                index++;
                return (T) objects1[index - 1];

            }
        };
        return iterator;
    }

    @Override
    public T[] toArray() {
        return objects;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (size >= objects.length * 2 / 3) {
            newSize(size);
        }
        objects[size] = t;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(this.objects[i])) {
                delHelper(i);
            }
            return true;
        }
        return false;
    }

    private void delHelper(int index) {
        for (int i = index; i < size; i++) {
            objects[i] = objects[i + 1];
            index = i;
        }
        objects[index + 1] = null;
    }

    private void newSize(int size) {
        Object newObject[] = new Object[size * 8 / 5 + 1];
        for (int i = 0; i < objects.length; i++) {
            newObject[i] = objects[i];
        }
        objects = (T[]) newObject;
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object x : c.toArray()) {
            if (!(contains(x))) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (Object x : c.toArray()) {
            add((T) x);
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object x : c.toArray()) {
            remove(x);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Object t : objects) {
            if (!c.contains(t)) {
                remove(t);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        this.size = 0;
    }

    @Override
    public boolean offer(T t) {
        if (size <= objects.length) {
            newSize(size);
        }
        objects[size++] = t;
        return true;
    }

    @Override
    public T remove() {
        if (size == 0) throw new NoSuchElementException();
        return objects[--size];
    }

    @Override
    public T poll() {
        return objects[--size];

    }

    @Override
    public T element() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        T el = objects[--size];
        return el;

    }

    @Override
    public T peek() {
        T el = objects[--size];
        return el;
    }
}
