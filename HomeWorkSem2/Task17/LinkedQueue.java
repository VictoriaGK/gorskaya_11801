package ru.itis.kpfu;

import java.util.*;

public class LinkedQueue<T> extends LinkedCollection<T> implements Queue<T> {


    @Override
    public boolean offer(T t) {
        Node<T> node = new Node<>(t);
        node.next = head;
        head = node;
        size++;
        return true;
    }

    @Override
    public T remove() {
        T value = last.value;
        this.last = null;
        return value;
    }

    @Override
    public T poll() {
        if (isEmpty()) throw new NoSuchElementException();
        T value = last.value;
        this.last = null;
        return value;
    }

    @Override
    public T element() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        T elem = this.last.value;
        last = null;
        return elem;
    }

    @Override
    public T peek() {
        return this.last.value;
    }
}
